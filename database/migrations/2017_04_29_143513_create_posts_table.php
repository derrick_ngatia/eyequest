<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id');
          $table->string('product_name', 50)->nullable();
          $table->string('bid_with', 50)->nullable();
          $table->string('location', 50)->nullable();
          $table->text('description');
          $table->string('image', 100)->nullable();
          $table->integer('category_id')->nullable();
          $table->integer('postCategory');
          $table->string('productId', 30)->nullable();
          $table->integer('bidders')->default(0);
          $table->tinyInteger('profileupdatepost')->default(0);
          $table->integer('suggestions')->default(0);
          $table->tinyInteger('question')->default(0);
          $table->tinyInteger('collected')->default(0);
          $table->tinyInteger('active')->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
