<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('email', 70)->unique();
            $table->string('password');
            $table->string('avatar', 100)->nullable();
            $table->integer('points')->default(0);
            $table->string('phone', 15);
            $table->tinyInteger('employed')->default(0);
            $table->string('location', 50);
            $table->tinyInteger('updatedprofile')->default(0);
            $table->string('talent', 200)->nullable();
            $table->text('cv')->nullable();
            $table->boolean('activated')->default(0);
            $table->string('token', 50)->default(null);
            $table->tinyInteger('userType')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
