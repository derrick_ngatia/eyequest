<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transactionCode', 10);
            $table->bigInteger('amount')->unsigned();
            $table->string('phone', 12);
            $table->integer('post_id');
            $table->integer('user_id');
            $table->integer('postCategory');
            $table->tinyInteger('confirmed')->default(0);
            $table->tinyInteger('failed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
