<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{

  protected $fillable = [
    'post_id', 'user_id', 'suggestion', 'description', 'image'
  ];

  public function user()
  {
    return $this->belongsTo('App\User');
  }

}
