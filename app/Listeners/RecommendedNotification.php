<?php

namespace App\Listeners;

use App\Events\Recommended;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Mail\RecommendedEmail;

class RecommendedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Recommended  $event
     * @return void
     */
    public function handle(Recommended $event)
    {
        Mail::to($event->recommendation->post->user->email)->send(new RecommendedEmail($event->recommendation));
    }
}
