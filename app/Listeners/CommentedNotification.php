<?php

namespace App\Listeners;

use App\Events\Commented;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Replies;
use App\Mail\CommentedEmail;

class CommentedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Commented  $event
     * @return void
     */
    public function handle(Commented $event)
    {
      if($event->reply->user->user_id != $event->reply->post->user->user_id){
        Mail::to($event->reply->post->user->email)->send(new CommentedEmail($event->reply));
      }
    }
}
