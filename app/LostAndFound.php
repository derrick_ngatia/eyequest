<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LostAndFound extends Model
{
    protected $fillable = [
          'name', 'user_id', 'image', 'description', 'productId', 'location'
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function payments()
    {
      return $this->hasMany('App\Payment', 'post_id');
    }
}
