<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  /*Using the second way */
   protected $fillable = [
     'user_id', 'bid_with', 'description', 'category_id', 'product_name', 'image', 'productId', 'location', 'collected', 'question', 'postCategory', 'profileupdatepost'
   ];

   public function payments()
   {
     return $this->hasMany('App\Payment');
   }
   public function bids()
   {
     return $this->hasMany('App\Bid');
   }
   public function user()
   {
     return $this->belongsTo('App\User');
   }

   public function suggestion()
   {
     return $this->hasMany('App\Suggestion');
   }

   public function replies(){
     return $this->hasMany('App\Replies', 'cv_posts_id');
   }

   public function recommends()
   {
     return $this->hasMany('App\Recommend', 'post_id');
   }

}
