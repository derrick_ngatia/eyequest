<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Replies;

class CommentEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public $reply;

     public $message;

    public function __construct(Replies $reply)
    {
        $this->reply = $reply;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $this->withSwiftMessage(function ($message) {
          $this->message = $message->getHeaders()
                  ->addTextHeader('Custom-Header', 'HeaderValue');
      });
        return $this->markdown('email.commentedEmail')
                    ->with([
                       'reply' => $this->reply,
                       'message' => $this->message
                   ]);

    }
}
