<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
  protected $fillable = [
      'transactionCode', 'phone', 'amount', 'post_id', 'user_id', 'postCategory'
  ];

  protected $hidden = [
      'transactionCode', 'phone', 'amount',
  ];

  public function post()
  {
    return $this->belongsTo('App\Post');
  }
}
