<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{

  protected $fillable = [
    'post_id', 'user_id', 'bid_with', 'image', 'description'
  ];

    public function user()
    {
       return $this->belongsTo('App\User');
    }

    public function post(){
      return $this->belongsTo('App\Post');
    }
}
