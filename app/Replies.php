<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Replies extends Model
{
    use Notifiable;
   
    protected $table = 'replies';

    protected $fillable = [
      'cv_posts_id', 'user_id', 'reply'
    ];

    //Events
    protected $dispatchesEvents = [
      'created' => Events\Commented::class
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function post()
    {
        return $this->belongsTo('App\Cv_Post', 'cv_posts_id');
    }
}
