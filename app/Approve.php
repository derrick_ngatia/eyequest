<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Approve extends Model
{
    protected $fillable = [
      'user_id', 'replies_id'
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }

}
