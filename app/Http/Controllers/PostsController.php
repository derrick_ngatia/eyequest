<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;
use App\Payment;
use Carbon\Carbon;
use App\Message;
use Session;
use App\Bid;
use App\Suggestion;
use DB;
use Image;


class PostsController extends Controller
{

    public function __construct()
    {
      $this->middleware(['auth', 'confirmation', 'updateProfile']);
    }

    public function add()
    {
        return view('pages.posts.addpost');
    }

    public function create(Request $request)
    {

      $post = new Post();

      $this->validate($request, [
        'product_name' => 'required|max:20',
        'bid_with' => 'required|max:100',
        'description' => 'required|max:200',
        'category' => 'required',
        'location' => 'required',
        'image' => 'nullable'
      ]);

      if($request->hasFile('image')){
        $extension = $request->image->extension();
        $name = str_replace([':', ' '], '-', Carbon::now()) .'-'.uniqid(). '.' . $extension;
        $dataType = array("jpg", "jpeg", "png");
        $path = public_path('/uploads/posts/'). $name;
        $uploaded = false;
        if(in_array($extension, $dataType)){
          ini_set ('gd.jpeg_ignore_warning', 1);
          error_reporting(E_ALL & ~E_NOTICE);
          $img = Image::make($request->file('image'));
          $size  = $img->filesize();
          if($size < 153600){ //150kb
            $img->save($path);
            $uploaded = true;
          }else {
            $img1 = Image::make($img->resize(300, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                }));
            if($img1->save($path)){
              $uploaded = true;
            }else{
              $uploaded = false;
            }
          }
        }else {
            $sucess = '<div class="alert alert-danger alert-dismissable"> Image Type not allowed </div>';
            Session::flash('message', $sucess);
            $post = Post::find($request->post_id);
            return redirect('/lost-and-found/add');
          }

        if($uploaded){
          $result = $post::create([
            'user_id' => Auth::User()->id,
            'product_name' => strtoupper($request->product_name),
            'bid_with' => $request->bid_with,
            'category_id' => $request->category,
            'description' => $request->description,
            'location' => $request->location,
            'postCategory' => 1,
            'productId' => strtoupper(uniqid()),
            'image' => $name
          ]);
          if($result){
            $sucess = '<div class="alert alert-success alert-dismissable"> Your post has been created </div>';
            Session::flash('message', $sucess);
            return redirect('/user');
          }else{
            $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to create your post at the moment, please try again </div>';
            Session::flash('message', $sucess);
            return redirect('/lost-and-found/add');
          }

        }else {
          $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to upload your image at the moment, please try again </div>';
          Session::flash('message', $sucess);
          return redirect('/lost-and-found/add');
        }
      }else {
        $result = $post::create([
          'user_id' => Auth::User()->id,
          'product_name' => strtoupper($request->product_name),
          'bid_with' => $request->bid_with,
          'category_id' => $request->category,
          'location' => $request->location,
          'productId' => strtoupper(uniqid()),
          'postCategory' => 3,
          'description' => $request->description
        ]);

        if($result){
            return redirect('/user');
        }else{
          $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to create your post at the moment, please try again </div>';
          Session::flash('message', $sucess);
          return redirect('/lost-and-found/add');
        }
      }
    }


    public function message(Request $request)
    {
      $post = new Message();

      $this->validate($request, [
        'name' => 'required|max:50',
        'subject' => 'required|max:50',
        'email' => 'required|max:200|email',
        'message' => 'required|max:900'
      ]);

      $result = $post::create([

        'name' => $request->name,
        'subject' => $request->subject,
        'email' => $request->email,
        'messages' => $request->message

      ]);
      if($result){
        $sucess = '<div class="alert alert-success alert-dismissable"> Message submitted successfully </div>';
        Session::flash('message', $sucess);
        return redirect('/contact');
      }else {
        $sucess = '<div class="alert alert-success alert-dismissable"> Failed to submit message at the moment please try again</div>';
        Session::flash('message', $sucess);
        return redirect('/contact');
      }
    }

    public function Bidders($id)
    {

        $posts =  Post::where('id', $id)
                        ->get();
        $payments = Post::find($id)->payments()
                          ->latest()
                          ->limit(1)
                          ->get();

        return view('pages.posts.bidders', compact('posts'));
    }

    public function Suggestions($id)
    {
      $posts =  Post::where('id', $id)
                      ->get();
      $payments = Post::find($id)->payments()
                        ->latest()
                        ->limit(1)
                        ->get();
      return view('pages.posts.suggestions', compact('posts'));
    }

    public function bidItem($id)
    {
      $id = $id;
      $post = Post::find($id);
      return view('pages.posts.mpesaBid', compact('id', 'post'));
    }

    public function suggestItem($id)
    {
      $id = $id;
      $post = Post::find($id);
      return view('pages.posts.mpesaSuggest', compact('id', 'post'));
    }

    public function add_bid(Request $request)
    {
      $this->validate($request, [
        'post_id' => 'required',
        'refNo' => 'required|max:10|min:10',
        'number' => 'required|max:15',
        'description' => 'required|max:200',
        'postCategory' => 'required',
        'bid_with' => 'required'
      ]);

      $result = Payment::create([
          'user_id' => Auth::User()->id,
          'post_id' => $request->post_id,
          'amount' => $request->amount,
          'postCategory' => $request->postCategory,
          'phone' => Auth::User()->phone,
          'transactionCode' => $request->refNo
          ]);

          if($result){
            $post = Post::find($request->post_id);
            $newBid = $post->bidders + 1;
            $post->bidders = $newBid;
            $post->save();
            if($request->hasFile('image')){
              $extension = $request->image->extension();
              $name = str_replace([':', ' '], '-', Carbon::now()) .'-'.uniqid(). '.' . $extension;
              $path = public_path('/uploads/bids/'). $name;
              $dataType = array("jpg", "jpeg", "png");
              $uploaded = false;
              if(in_array($extension, $dataType)){
                ini_set ('gd.jpeg_ignore_warning', 1);
                error_reporting(E_ALL & ~E_NOTICE);
                $img = Image::make($request->file('image'));
                $size  = $img->filesize();
                if($size < 153600){ //150kb
                  $img->save($path);
                  $uploaded = true;
                }else {
                  $img1 = Image::make($img->resize(300, null, function ($constraint) {
                                          $constraint->aspectRatio();
                                      }));
                  if($img1->save($path)){
                    $uploaded = true;
                  }else{
                    $uploaded = false;
                  }
                }
              }else {
                $sucess = '<div class="alert alert-danger alert-dismissable"> Image Type Npt allowed </div>';
                Session::flash('message', $sucess);
                $post = Post::find($request->post_id);
                return view('pages.posts.mpesaBid', compact('post'));
              }
              if($uploaded){
                $bid = Bid::create([
                  'post_id' => $request->post_id,
                  'user_id' => Auth::User()->id,
                  'bid_with' => $request->bid_with,
                  'description' => $request->description,
                  'image' => $name
                ]);

                if($bid){
                  $sucess = '<div class="alert alert-success alert-dismissable"> Bid Created successfully, After your payment is approved your bid will be visible and owner number </div>';
                  Session::flash('message', $sucess);
                  $post = Post::find($request->post_id);
                  return view('pages.posts.mpesaBid', compact('post'));
                }else {
                  $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to create a bid at the moment </div>';
                  Session::flash('message', $sucess);
                  $post = Post::find($request->post_id);
                  return view('pages.posts.mpesaBid', compact('post'));
                }
              }else {
                $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to upload the image at the moment </div>';
                Session::flash('message', $sucess);
                $post = Post::find($request->post_id);
                return view('pages.posts.mpesaBid', compact('post'));
              }
            }else {
              $bid = Bid::create([
                'post_id' => $request->post_id,
                'user_id' => Auth::User()->id,
                'bid_with' => $request->bid_with,
                'description' => $request->description
              ]);

              if($bid){
                $sucess = '<div class="alert alert-success alert-dismissable"> Bid Created successfully, After your payment is approved your bid will be visible and owner number </div>';
                Session::flash('message', $sucess);
                $post = Post::find($request->post_id);
                return view('pages.posts.mpesaBid', compact('post'));
              }else {
                $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to create a bid at the moment </div>';
                Session::flash('message', $sucess);
                $post = Post::find($request->post_id);
                return view('pages.posts.mpesaBid', compact('post'));
              }
            }
          }else {
            $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to create a bid at the moment </div>';
            Session::flash('message', $sucess);
            $post = Post::find($request->post_id);
            return view('pages.posts.mpesaBid', compact('post'));
          }
    }

    public function add_suggestion(Request $request)
    {
      $this->validate($request, [
        'post_id' => 'required',
        'refNo' => 'required|max:10|min:10',
        'number' => 'required|max:15',
        'postCategory' => 'required',
        'suggestion' => 'required',
        'description' => 'required|max:200'

      ]);

      $result = Payment::create([
          'user_id' => Auth::User()->id,
          'post_id' => $request->post_id,
          'amount' => $request->amount,
          'postCategory' => $request->postCategory,
          'phone' => Auth::User()->phone,
          'transactionCode' => $request->refNo
          ]);

          if($result){
            $post = Post::find($request->post_id);
            $newBid = $post->suggestions + 1;
            $post->suggestions = $newBid;
            $post->save();

            if($request->hasFile('image')){
              $extension = $request->image->extension();
              $name = str_replace([':', ' '], '-', Carbon::now()) .'-'.uniqid(). '.' . "jpeg";
              $path = public_path('/uploads/suggestions/'). $name;
              $dataType = array("jpg", "jpeg", "png");
              $uploaded = false;
              if(in_array($extension, $dataType)){
                ini_set ('gd.jpeg_ignore_warning', 1);
                error_reporting(E_ALL & ~E_NOTICE);
                $img = Image::make($request->file('image'));
                $size  = $img->filesize();
                if($size < 153600){ //150kb
                  $img->save($path);
                  $uploaded = true;
                }else {
                  $img1 = Image::make($img->resize(400, null, function ($constraint) {
                                          $constraint->aspectRatio();
                                      }));
                  if($img1->save($path)){
                    $uploaded = true;
                  }else{
                    $uploaded = false;
                  }
                }
              }else {
                $sucess = '<div class="alert alert-danger alert-dismissable"> Image Type Not allowed </div>';
                Session::flash('message', $sucess);
                $post = Post::find($request->post_id);
                return view('pages.posts.mpesaSuggest', compact('post'));
              }

              if($uploaded){
                $suggest = Suggestion::create([
                  'post_id' => $request->post_id,
                  'user_id' => Auth::User()->id,
                  'suggestion' => $request->suggestion,
                  'description' => $request->description,
                  'image' => $name
                ]);

                if($suggest){
                  $sucess = '<div class="alert alert-success alert-dismissable"> Suggestion Created successfully, After your payment is approved your suggestion will be visible and owner number </div>';
                  Session::flash('message', $sucess);
                  $post = Post::find($request->post_id);
                  return view('pages.posts.mpesaSuggest', compact('post'));
                }else {
                  $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to create a suggestion at the moment </div>';
                  Session::flash('message', $sucess);
                  $post = Post::find($request->post_id);
                  return view('pages.posts.mpesaSuggest', compact('post'));
                }

              }else {
                $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to upload the image at the moment </div>';
                Session::flash('message', $sucess);
                $post = Post::find($request->post_id);
                return view('pages.posts.mpesaSuggest', compact('post'));
              }
            }else {
              $suggest = Suggestion::create([
                'post_id' => $request->post_id,
                'user_id' => Auth::User()->id,
                'suggestion' => $request->suggestion,
                'description' => $request->description
              ]);

              if($suggest){
                $sucess = '<div class="alert alert-success alert-dismissable"> Suggestion Created successfully, After your payment is approved your suggestion will be visible and owner number </div>';
                Session::flash('message', $sucess);
                $post = Post::find($request->post_id);
                return view('pages.posts.mpesaSuggest', compact('post'));
              }else {
                $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to create a suggestion at the moment </div>';
                Session::flash('message', $sucess);
                $post = Post::find($request->post_id);
                return view('pages.posts.mpesaSuggest', compact('post'));
              }
            }
          }else {
            $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to create a suggestion at the moment </div>';
            Session::flash('message', $sucess);
            $post = Post::find($request->post_id);
            return view('pages.posts.mpesaSuggest', compact('post'));
          }


    }
}
