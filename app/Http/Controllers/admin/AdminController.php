<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\User;
use App\Message;
use Auth;
use App\Payment;
use Carbon\Carbon;
use Session;
use App\Admin;

class AdminController extends Controller
{
  public function __construct()
  {
      $this->middleware(['auth', 'confirmation', 'updateProfile']);
  }

  public function index()
  {
    $users = User::where('userType', 0)
            ->orderBy('created_at', 'desc')
            ->paginate(5);
    $messages = Message::paginate(5);
    return view('pages.admin.admin', compact(['users', 'messages']));
  }

  public function payments()
  {
      $payments = Payment::orderBy('created_at', 'desc')
                          ->paginate(5);
      return view('pages.admin.payments', compact('payments'));
  }

  public function approve($id)
  {
    $payment = Payment::find($id);

    $payment->confirmed = 1;

    $payment->save();

    $payments = Payment::orderBy('created_at', 'desc')
                        ->paginate(5);
    return view('pages.admin.payments', compact('payments'));
  }

  public function cancel($id)
  {
      $payment = Payment::find($id);

      $payment->failed = 1;

      $payment->save();

      $payments = Payment::orderBy('created_at', 'desc')
                          ->paginate(5);
      return view('pages.admin.payments', compact('payments'));
  }

  public function adertPan(Request $request){

    if($request->hasFile('image')){
      $extension = $request->image->extension();
      $name = 'admin.' . $extension;
      $file = $file = $request->file('image');
      $uploaded = $request->image->storeAs('uploads/admin', $name, 'uploads');
      if($uploaded){
        $admin = Admin::find(1);
        $admin->image = $name;
        $admin->save();
        $sucess = '<div class="alert alert-success alert-dismissable"> Image successfully uploaded </div>';
        Session::flash('message', $sucess);
        return redirect('/admin');
      }else {
        $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to upload image at the moment, please try again </div>';
        Session::flash('message', $sucess);
        return redirect('/admin');
      }
  }
}


}
