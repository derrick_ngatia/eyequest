<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'confirmation', 'updateProfile']);
    }


    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')
                  ->where('active', 1)
                  ->where('postCategory', '!=', 3)
                  ->paginate(6);
        return view('pages.home', compact('posts'));
    }

    public function categories($id)
    {
      $posts = Post::where('category_id', $id)
                    ->where('active', 1)
                    ->paginate(12);
      return view('pages.home', compact('posts'));
    }

    public function search(Request $request)
    {


      if($request->srch == 1){
        $posts = Post::orderBy('created_at', 'desc')
                ->where('active', 1)
                ->where('postCategory', '!=', 3)
                ->where('product_name', 'LIKE', '%'.$request->search .'%')
                ->paginate(15);

        return view('pages.search', compact('posts'));
      }else{
        $posts = User::orderBy('points', 'desc')
                ->where('name', 'LIKE', '%'.$request->search .'%')
                ->orWhere('talent', 'LIKE', '%'.$request->search .'%')
                ->distinct()
                ->paginate(20);

        return view('pages.userSearch', compact('posts'));
      }


    }
}
