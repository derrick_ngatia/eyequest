<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Bid;
use App\Payment;
use App\User;
use Auth;
use Carbon\Carbon;
use App\Follower;
use Session;
use Image;


class UsersController extends Controller
{
  public function __construct()
  {
      $this->middleware(['auth', 'confirmation', 'updateProfile']);
  }

  public function confirmEmail($id, $token)
  {

    echo $id . " ". $token;
  /*  $user = User::where('id', $id)
                  ->where('token', $token)
                  ->first();

      $user->activated = 1;

      if($user->save()){
        echo $user. "<br>";
        echo "Url ID: ". $id . "<br>";
        echo "URL token: " . $token . "<br>";
        echo "Saved";
        //return redirect('/');
      }else{
        echo $user. "<br>";
        echo "Url ID: ". $id . "<br>";
        echo "URL token: " . $token . "<br>";
        echo "Failed to save";
        //return redirect('/confirm');
      }*/

  }

  public function getUser()
  {
    $posts = Post::where('user_id', Auth::User()->id )
              ->where('postCategory', '!=', 3)
              ->orderBy('created_at', 'desc') //desc
              ->paginate(6);
    return view('pages.user.user', compact('posts'));
  }

  public function get_bids()
  {
    $bids = Bid::where('user_id', Auth::User()->id)
                ->latest()
                ->paginate(6);
    return view('pages.user.userBids', compact('bids'));
  }

  public function get_transactions()
  {
    $payments = Payment::where('user_id', Auth::User()->id)
                        ->latest()
                        ->paginate(6);
     return view('pages.user.userTransactions', compact('payments'));
  }

  public function profile()
  {
    $user = User::find(Auth::User()->id);
    $id = Auth::User()->id;
    $trade = Post::where('postCategory', 1)
                   ->where('user_id', $id)
                   ->latest()
                   ->get();
    $losts = Post::where('postCategory', 2)
                   ->where('user_id', $id)
                   ->latest()
                   ->get();
    $talents = Post::where('postCategory', 3)
                     ->where('user_id', $id)
                     ->latest()
                     ->paginate(10);

    $followers = Follower::where('followed_id', $id)->get();
    $following = Follower::where('user_id', $id)->get();

    return view('pages.user.profile', compact('user', 'losts', 'talents', 'trade'));
  }

  public function get_user($name, $id)
  {

     $user = User::find($id);
     $trade = Post::where('postCategory', 1)
                    ->where('user_id', $id)
                    ->latest()
                    ->get();
     $losts = Post::where('postCategory', 2)
                    ->where('user_id', $id)
                    ->latest()
                    ->get();
     $talents = Post::where('postCategory', 3)
                      ->where('user_id', $id)
                      ->latest()
                      ->paginate(10);
     $followers = Follower::where('followed_id', $id)->get();
     $following = Follower::where('user_id', $id)->get();

     return view('pages.talents.single', compact('user', 'losts', 'talents', 'trade', 'followers', 'following'));
  }

  public function create_prof(Request $request)
  {

     $this->validate($request, [
       'image' => 'required'
     ]);

     //If validation fails redirect back with validation
    /* if($validator->fails())
       {
       return Redirect::back()->withErrors($validator);
     } */

     if($request->hasFile('image')){
       $size = $request->file('image')->getClientSize();

       $extension = $request->image->extension();
       $name = str_replace([':', ' '], '-', Carbon::now()) .'-'.uniqid(). '.' . $extension;
       $path = public_path('/uploads/avatars/'). $name;
       $dataType = array("jpg", "jpeg", "png");
       $uploaded = false;
       if(in_array($extension, $dataType)){
         ini_set ('gd.jpeg_ignore_warning', 1);
         error_reporting(E_ALL & ~E_NOTICE);
         $img = Image::make($request->file('image'));
         $size  = $img->filesize();
         
         if(isset(Auth::User()->image)){
           unlink(public_path('uploads/avatars/'. Auth::User()->image ));
         }

        if($size < 102400){ //100kb
            $img->save($path);
            $uploaded = true;
          }else {
            $img1 = Image::make($img->resize(300, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                }));
            if($img1->save($path)){
              $uploaded = true;
            }else{
              $uploaded = false;
            }
          }
       }else {
         $sucess = '<div class="alert alert-danger alert-dismissable"> Image Type Not allowed </div>';
         Session::flash('messageedit', $sucess);
         return redirect('/profile');
       }
       if($uploaded){
         $user = User::find(Auth::User()->id);

        $user->avatar = $name;

        $user->save();

         return redirect('/profile');
       }else {
         $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to update image at the moment, please try again </div>';
         Session::flash('messageedit', $sucess);
         return redirect('/profile');
       }
     }else {
       $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to update image at the moment, please try again </div>';
       Session::flash('messageedit', $sucess);
       return redirect('/profile');
     }
  }

    public function edit_profile(Request $request){

      $this->validate($request, [
        'name' => 'required|max:50',
        'description' => 'required|max:300',
        'talent' => 'required|max:50',
        'employed' => 'required'
      ]);

      //If validation fails redirect back with validation
      /*if($validator->fails())
        {
        return Redirect::back()->withErrors($validator);
        }*/

        $user = User::find(Auth::User()->id);

        $user->talent = $request->talent;
        $user->cv = $request->description;
        $user->employed = $request->employed;
        $user->name = $request->name;

        if($user->save()){
          $sucess = '<div class="alert alert-danger alert-dismissable"> Profile edited successfully </div>';
          Session::flash('messageedit', $sucess);
          return redirect('/profile');
        }else {
          $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to edit profile at the moment, Please Try again later </div>';
          Session::flash('messageedit', $sucess);
          return redirect('/profile');
        }
    }
}
