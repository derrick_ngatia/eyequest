<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Carbon\Carbon;
use Session;
use App\Post as Cv_Post;
use Image;
use Illuminate\Support\Facades\Input;

class UpdateProfileController extends Controller
{
  public function __construct()
  {
      $this->middleware(['auth', 'confirmation']);
  }


  public function updateImage(Request $request) {
    if($request->hasFile('image')){
      $size = $request->file('image')->getClientSize();

      $extension = $request->image->extension();
      $name = str_replace([':', ' '], '-', Carbon::now()) .'-'.uniqid(). '.' . $extension;
      $path = public_path('/uploads/avatars/'). $name;
      $dataType = array("jpg", "jpeg", "png");
      $uploaded = false;
      if(in_array($extension, $dataType)){
        ini_set ('gd.jpeg_ignore_warning', 1);
        error_reporting(E_ALL & ~E_NOTICE);
        $img = Image::make($request->file('image'));
        $size  = $img->filesize();

        if(isset(Auth::User()->image)){
          unlink(public_path('uploads/avatars/'. Auth::User()->image ));
        }

       if($size < 102400){ //100kb
           $img->save($path);
           $uploaded = true;
         }else {
           $img1 = Image::make($img->resize(300, null, function ($constraint) {
                                   $constraint->aspectRatio();
                               }));

           if($img1->save($path)){
             $uploaded = true;
           }else{
             $uploaded = false;
           }
         }
      }else {
        
        return false;
      }
      if($uploaded){
        $user = User::find(Auth::User()->id);

       $user->avatar = $name;

       $user->save();

        return true;
      }else {

        return false;
      }
    }else {
      return false;
    }
  }

  public function createPost(Request $request){

    $post = new Cv_Post();

    if($request->hasFile('image')){
      $extension = $request->image->extension();
      $name = str_replace([':', ' '], '-', Carbon::now()) .'-'.uniqid(). '.' . $extension;
      $path = public_path('/uploads/cvposts/'). $name;
      $dataType = array("jpg", "jpeg", "png");
      $uploaded = false;
      if(in_array($extension, $dataType)){
        ini_set ('gd.jpeg_ignore_warning', 1);
        error_reporting(E_ALL & ~E_NOTICE);
        $file = Input::file('image');
        $img = null;
       if ($request->file('image')->isValid()) {
          $img = Image::make($file);
        }



        $size  = $img->filesize();
       if($size < 153600){ //150kb
          $img->save($path);
          $uploaded = true;
        }else {
          $img1 = Image::make($img->resize(600, null, function ($constraint) {
                                  $constraint->aspectRatio();
                              }));
          if($img->save($path)){
            $uploaded = true;
          }else{
            $uploaded = false;
          }
        }
      }else {
        return false;
      }
      if($uploaded){
        $result = $post::create([
          'user_id' => Auth::User()->id,
          'description' => $request->description,
          'image' => $name,
          'postCategory' => 3,
          'profileupdatepost' => 1
        ]);

        if($result){
          return true;
        }else{
          return false;
        }
      }else {
        return false;
      }


    }else {
      $result = $post::create([
        'user_id' => Auth::User()->id,
        'postCategory' => 3,
        'description' => $request->description
      ]);

      if($result){
        return true;
      }else{
        return true;
      }
    }
  }

  public function updateProfile(Request $request){

    $this->validate($request, [
      'name' => 'required|max:50',
      'description' => 'required|max:300',
      'talent' => 'required|max:50',
      'employed' => 'required',
      'image' => 'required'
    ]);

   
    $updatedimage = $this->updateImage($request);
    $createdPost = $this->createPost($request);
    if($updatedimage && $createdPost){
      $user = User::find(Auth::User()->id);

      $user->talent = $request->talent;
      $user->cv = $request->description;
      $user->employed = $request->employed;
      $user->name = $request->name;
      $user->updatedprofile = 1;


      if($user->save()){
        $sucess = '<div class="alert alert-danger alert-dismissable"> Profile edited successfully </div>';
        Session::flash('messageedit', $sucess);
        return redirect(url('/'));
      }else {
        $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to edit profile at the moment, Please Try again later </div>';
        Session::flash('messageedit', $sucess);
        return redirect(url('/updateprofile'));
      }
    }else {
      echo "Failed to create post or profile image";
    }

  }
}
