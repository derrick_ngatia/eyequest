<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post as Cv_Post;
use Carbon\Carbon;
use Auth;
use Session;
use App\Replies;
use App\Follower;
use App\Recommend;
use App\Approve;
use Image;
use Illuminate\Support\Facades\Input;

class TalentsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'confirmation', 'updateProfile']);
    }

    public function index()
    {
      $posts = Cv_Post::where('postCategory', 3)
                ->latest()->paginate(10);
      return view('pages.talents.home', compact('posts'));
    }

    public function create(Request $request)
    {
      $this->validate($request, [
        'description' => 'required'
      ]);

      $post = new Cv_Post();

      if($request->hasFile('image')){
        $extension = $request->image->extension();
        $name = str_replace([':', ' '], '-', Carbon::now()) .'-'.uniqid(). '.' . $extension;
        $path = public_path('/uploads/cvposts/'). $name;
        $dataType = array("jpg", "jpeg", "png");
        $uploaded = false;
        if(in_array($extension, $dataType)){
          ini_set ('gd.jpeg_ignore_warning', 1);
          error_reporting(E_ALL & ~E_NOTICE);
          $file = Input::file('image');
          $img = null;
         if ($request->file('image')->isValid()) {
    		$img = Image::make($file);
	 }



          $size  = $img->filesize();
         if($size < 153600){ //150kb
            $img->save($path);
            $uploaded = true;
          }else {
            $img1 = Image::make($img->resize(600, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                }));
            if($img->save($path)){
              $uploaded = true;
            }else{
              $uploaded = false;
            }
          }
        }else {
          $sucess = '<div class="alert alert-danger alert-dismissable"> Image Type Not allowed </div>';
          Session::flash('message', $sucess);
          return redirect('/profession');
        }
        if($uploaded){
          $result = $post::create([
            'user_id' => Auth::User()->id,
            'description' => $request->description,
            'image' => $name,
            'postCategory' => 3
          ]);
          if($result){
            $sucess = '<div class="alert alert-success alert-dismissable"> Your post has been created </div>';
            Session::flash('message', $sucess);
            return redirect('/profession');
          }else{
            $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to create your post at the moment, please try again </div>';
            Session::flash('message', $sucess);
            return redirect('/profession');
          }
        }else {
          $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to upload image at the moment, please try again </div>';
          Session::flash('message', $sucess);
          return redirect('/profession');
        }


      }else {
        $result = $post::create([
          'user_id' => Auth::User()->id,
          'postCategory' => 3,
          'description' => $request->description
        ]);

        if($result){
          $sucess = '<div class="alert alert-success alert-dismissable"> Your post has been created </div>';
          Session::flash('message', $sucess);
          return redirect('/profession');
        }else{
          $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to create your post at the moment, please try again </div>';
          Session::flash('message', $sucess);
          return redirect('/profession');
        }
      }
    }

    public function post_Question(Request $request){
      $this->validate($request, [
        'description' => 'required'
      ]);

      $post = new Cv_Post();

      if($request->hasFile('image')){
        $extension = $request->image->extension();
        $name = str_replace([':', ' '], '-', Carbon::now()) .'-'.uniqid(). '.' . $extension;
        $path = public_path('/uploads/cvposts/'). $name;
        $dataType = array("jpg", "jpeg", "png");
        $uploaded = false;
        if(in_array($extension, $dataType)){
          ini_set ('gd.jpeg_ignore_warning', 1);
          error_reporting(E_ALL & ~E_NOTICE);
          $img = Image::make($request->file('image'));
          $size  = $img->filesize();
          if($size < 153600){
            $img->save($path);
            $uploaded = true;
          }else {
            $img1 = Image::make($img->resize(600, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                }));
            if($img1->save($path)){
              $uploaded = true;
            }else{
              $uploaded = false;
            }
          }
        }else {
          $sucess = '<div class="alert alert-danger alert-dismissable"> Image Type Not allowed </div>';
          Session::flash('message', $sucess);
          return redirect('/profession');
        }
        if($uploaded){
          $result = $post::create([
            'user_id' => Auth::User()->id,
            'description' => $request->description,
            'image' => $name,
            'postCategory' => 3,
            'question' => 1
          ]);
          if($result){
            $sucess = '<div class="alert alert-success alert-dismissable"> Your post has been created </div>';
            Session::flash('message', $sucess);
            return redirect('/profession');
          }else{
            $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to create your post at the moment, please try again </div>';
            Session::flash('message', $sucess);
            return redirect('/profession');
          }
        }else {
          $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to upload image at the moment, please try again </div>';
          Session::flash('message', $sucess);
          return redirect('/profession');
        }


      }else {
        $result = $post::create([
          'user_id' => Auth::User()->id,
          'description' => $request->description,
          'postCategory' => 3,
          'question' => 1
        ]);

        if($result){
          $sucess = '<div class="alert alert-success alert-dismissable"> Your post has been created </div>';
          Session::flash('message', $sucess);
          return redirect('/profession');
        }else{
          $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to create your post at the moment, please try again </div>';
          Session::flash('message', $sucess);
          return redirect('/profession');
        }
      }
    }

    public function post_recommend($id){
      $reco = Recommend::where('user_id', Auth::User()->id)
                        ->where('post_id', $id)->get();

      if(count($reco) > 0){
        return response()->json(false);
      }else {
        $post = new Recommend();
        $pointer= Cv_Post::find($id)->user;

        $pointer->points = $pointer->points + 1;

        if($pointer->save()){
          $result = $post::create([
            'user_id' => Auth::User()->id,
            'post_id' => $id
          ]);

          if($result){
            return response()->json(1);
          }else{
            return response()->json(0);
          }
        }else {
          return response()->json(0);
        }
      }
    }

    public function reply_approve($reply_id, $user_id){
      $reco = Approve::where('user_id', $user_id)
                        ->where('replies_id', $reply_id )->get();

      if(count($reco) > 0){
        return response()->json(false);
      }else {
        $post = new Approve();
        $pointer= Replies::find($reply_id)->user;
        //return response()->json($pointer);
        $pointer->points = $pointer->points + 10;

        if($pointer->save()){
          $result = $post::create([
            'user_id' => $user_id,
            'replies_id' => $reply_id
          ]);

          if($result){
            return response()->json(1);
          }else{
            return response()->json(0);
          }
        }else {
          return response()->json(0);
        }
      }
    }

    public function create_comments(Request $request)
    {
        $post = new Replies();
        $result = $post::create([
          'cv_posts_id' => $request->post_id,
          'user_id' => Auth::User()->id,
          'reply' => $request->comment
        ]);

        if($result){
          $data = $request->all();
          return response()->json($data);
        }else {
          return response()->json(false);
        }
    }

    public function follow($id)
    {
      $post = new Follower();
      $result = $post::create([
        'user_id' => Auth::User()->id,
        'followed_id' => $id
      ]);

      if($result){

        return response()->json(true);
      }else {
        return response()->json(false);
      }

    }
}
