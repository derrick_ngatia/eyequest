<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post as Lost;
use Auth;
use Carbon\Carbon;
use Session;
use App\Payment;
use DB;
use Image;

class LostAndFoundController extends Controller
{

    public function __construct()
    {
      $this->middleware(['auth', 'confirmation', 'updateProfile']);

    }

    public function show()
    {
      $losts = Lost::where('active', 1)
                ->where('postCategory', 2)
                ->orderBy('created_at', 'desc')
                ->paginate(6);
      return view('pages.lostAndFound.lostFound', compact('losts'));
    }

    public function create()
    {
      return view('pages.lostAndFound.lostCreate');
    }

    public function add(Request $request)
    {

      $post = new Lost();
      $this->validate($request, [
        'product_name' => 'required|max:20',
        'description' => 'required|max:200',
        'location' => 'required|max:50',
        'image' => 'nullable'
      ]);

      if($request->hasFile('image')){
        $extension = $request->image->extension();
        $name = str_replace([':', ' '], '-', Carbon::now()) .'-'.uniqid(). '.' . $extension;
        $path = public_path('/uploads/losts/'). $name;
        $dataType = array("jpg", "jpeg", "png");
        $uploaded = false;
        if(in_array($extension, $dataType)){
          ini_set ('gd.jpeg_ignore_warning', 1);
          error_reporting(E_ALL & ~E_NOTICE);
          $img = Image::make($request->file('image'));
          $size  = $img->filesize();
          if($size < 153600){ //150kb
            $img->save($path);
            $uploaded = true;
          }else {
            $img1 = Image::make($img->resize(400, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                }));
            if($img1->save($path)){
              $uploaded = true;
            }else{
              $uploaded = false;
            }
          }
        }else {
          $sucess = '<div class="alert alert-danger alert-dismissable"> Image Type Not allowed </div>';
          Session::flash('message', $sucess);
          return redirect('/lost-and-found/add');
        }

        if($uploaded){
          $result = $post::create([
            'user_id' => Auth::User()->id,
            'product_name' => strtoupper($request->product_name),
            'description' => $request->description,
            'productId' => strtoupper(uniqid()),
            'postCategory' => 2,
            'location' => $request->location,
            'image' => $name
          ]);

          if($result){
            return redirect('/lost-and-found'); //->with('errors', 'Failed to upload image');
          }else{
            $sucess = '<div class="alert alert-danger alert-dismissable"> Unable to add post, Please try again later </div>';
            Session::flash('message', $sucess);
            return redirect('/lost-and-found/add'); //->with('errors', 'Failed to upload image');
          }

      }else {
          $sucess = '<div class="alert alert-danger alert-dismissable"> Unable to upload image </div>';
          Session::flash('message', $sucess);
          return redirect('/lost-and-found/add');
      }
    }else {
      $result = $post::create([
        'user_id' => Auth::User()->id,
        'product_name' => strtoupper($request->product_name),
        'productId' => strtoupper(uniqid()),
        'location' => $request->location,
        'postCategory' => 2,
        'description' => $request->description
      ]);

      if($result){
        return redirect('/lost-and-found');
      }else{
        $sucess = '<div class="alert alert-danger alert-dismissable"> Unable to add post at the moment, Please try later </div>';
        Session::flash('message', $sucess);
        return redirect('/lost-and-found/add');
      }
    }
}

  public function get_single($id)
  {
    $posts = Lost::where('id', $id)->get();
    $payments = Lost::find($id)->payments()
                      ->where('postCategory', 0)
                      ->where('user_id', Auth::User()->id)
                      ->latest()
                      ->limit(1)
                      ->first();

    return view('pages.lostAndFound.lost', compact('posts', 'payments'));
  }

  public function confirmPayment($id)
  {
    $id = $id;

    return view('pages.lostAndFound.mpesaLost', compact('id'));
  }

  public function savePayment(Request $request)
  {
    $this->validate($request, [
      'post_id' => 'required',
      'refNo' => 'required|max:10|min:10',
      'number' => 'required|max:15',
      'postCategory' => 'required'
    ]);

    $result = Payment::create([
        'user_id' => Auth::User()->id,
        'post_id' => $request->post_id,
        'amount' => $request->amount,
        'postCategory' => $request->postCategory,
        'phone' => Auth::User()->phone,
        'transactionCode' => $request->refNo
        ]);

        if($result){
            $sucess = '<div class="alert alert-success alert-dismissable"> Payment Created successfully, After your payment is approved follow up detailes will be visible </div>';
            Session::flash('message', $sucess);
            $id = $request->post_id;
            return view('pages.lostAndFound.mpesaLost', compact('id'));
        }else {
          $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to transact at the moment </div>';
          Session::flash('message', $sucess);
          $id = $request->post_id;
          return view('pages.lostAndFound.mpesaLost', compact('id'));
        }
  }
}
