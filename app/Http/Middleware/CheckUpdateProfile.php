<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckUpdateProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(Auth::User()->updatedprofile == 0){
        return redirect('/updateprofile');
      }
        return $next($request);
    }
}
