<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class ConfirmEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (Auth::User()->activated == 0) {
          return redirect('/confirm');
      }
        return $next($request);
    }
}
