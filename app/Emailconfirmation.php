<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emailconfirmation extends Model
{
    protected $fillable = [
      'user_id', 'token'
    ];
    
    protected $table = 'emailconfirmations';
}
