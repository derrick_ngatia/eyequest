<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Recommend extends Model
{
    use Notifiable;

    protected $fillable = [
      'user_id', 'post_id'
    ];
    
    protected $dispatchesEvents = [
      'created' => Events\Recommended::class
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function post()
    {
        return $this->belongsTo('App\Cv_Post', 'post_id');
    }

}
