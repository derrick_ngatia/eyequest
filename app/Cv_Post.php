<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cv_Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
            'body', 'image', 'user_id'
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function replies(){
      return $this->hasMany('App\Replies', 'cv_posts_id');
    }
}
