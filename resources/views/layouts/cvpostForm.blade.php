<div class="cvForm">
  <form class="form-horizontal" role="form" method="POST" action="/talents" enctype="multipart/form-data">
    @if(Session::has('message'))
      {!! Session::get('message') !!}
    @endif
      {{ csrf_field() }}
      <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
          <label for="description" class="col-md-4 control-label">Post Description</label>

          <div class="col-md-8">

              <textarea id="description" type="text" rows="2" class="form-control" name="description" value="" required>{{ old('description') }}</textarea>

              @if ($errors->has('description'))
                  <span class="help-block">
                      <strong>{{ $errors->first('description') }}</strong>
                  </span>
              @endif
          </div>
      </div>

      <div class="form-group{{ $errors->has('product_name') ? ' has-error' : '' }}">
        <label for="description" class="col-md-4 control-label"></label>
        <div class="col-md-8">
          <span class="btn btn-default btn-file">
            Post Image <input type="file" name="image" class="">
          </span>
        </div>

      </div>

      <div class="form-group">
          <div class="col-md-6 col-md-offset-4">
              <button type="submit" class="btn btn-primary submit">
                  Create Post
              </button>
              <button type="reset" class="btn btn-default">Reset Fields</button>
          </div>
      </div>
  </form>
</div>
