<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Eyequest trade lost and found Profession">
    <meta name="keywords" content="eyequest trade lostandfound Profession" />

    <title> Home | Eye-Quest </title>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!--<link href="/css/bootstrap.css" rel="stylesheet"> -->
    <link href="/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--<link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css"> -->



  <script src="/js/vde.js"></script>
  <script src="/js/jquery.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!--<script src="/js/bootstrap.js"></script> -->

  <script type="text/javascript">
            $(document).ready(function () {
            window.setTimeout(function() {
                $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
                    $(this).remove();
                });
            }, 5000);
            });
    </script>
    </head>
      <body>
        <div id="app">
          @include('layouts.topNav')
          <div class="content">
            @yield('content')
          </div>

        </div>
      </body>


    <!-- Scripts -->
   <script src="/js/main.js"></script>
</body>
</html>
