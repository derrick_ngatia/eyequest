@include('layouts.navbar')
<div class="container midNav">
  <div class="row">
    <img src="/images/logo/eyquest.jpg" alt="logo" class="logo">
    <div class="pull-right topRight">
        @if(isset(Auth::User()->name))
          <a href="/profile" class="profLink"> @if(isset(Auth::User()->avatar))
                <img src="{{ url('/Eyequest/public/uploads/avatars/'.Auth::User()->avatar)  }}" alt="Eye-Quest" class="profileavatar">
                @else
                  <img src="{{ url('images/logo/eyquest.jpg') }}" alt="Eye-Quest" class="profileavatar">
                @endif
            &nbsp; {{ Auth::User()->name }} &nbsp;
          <a href="{{ route('logout') }}"
              onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
              Logout
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
          </form>
        @else
          Welcome: Guest &nbsp;
          <a href="/login"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span>Login</a>
        @endif
    </div>
  </div>
  <div class="row">
    <div class=" topNav">
      <ul>
        <li>  <div class="topItem"><span class="glyphicon glyphicon-earphone topItemGly" aria-hidden="true"></span> (+254) 704176341</div></li>
        <li>  <div class="topItem"><span class="glyphicon glyphicon-envelope topItemGly" aria-hidden="true"></span> info@eyequest.co.ke</div></li>
        <li>
        <!--  <div class="topItem"> -->
          <div class="post-drop">
           <a href="#" class="post-item">Create Post</a>
            <ul class="post-dropdown">
              <li><a href="/post/add">Barter Trade</a></li>
              <li><a href="/lost-and-found/add">Lost and Found</a></li>
              <li><a href="#" id="cvDrop">Profession</a></li>
            </ul>
            </div>
          <!--</div> -->
        </li>
      </ul>
      
    </div>
  </div>
  <div class="row">

  </div>

</div>
