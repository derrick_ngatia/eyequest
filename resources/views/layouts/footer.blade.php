<div class="container footer">
  <div class="row footRow">
    <div class="col-md-12">
      <div class="col-md-3">
        <h3> <span class="faintColor">Eye</span>-Quest</h3>
        <p>EyeQuest is an online business environment that offers  the ultimate platform where users are able 
        to display their skills, products, look for jobs and  professional profiling  
        besides learning from each other, ...
            <a href="/about-us">READ MORE</a>
          </p>
      </div>
      <div class="col-md-6">
        <h3>Customer Comments</h3>

      </div>
      <div class="col-md-3 locationFooter" style="background-image: #f76801 url({{ url('images/logo/world-map.jpg') }});">
        <h3>Location</h3>
        Upper Industrial Area, Printing Press Road, P.O Box 18100-20100 Nakuru
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-3">
        <h3>SERVICES</h3>
        <ul class="footerList">
          <li><a href="#">Online Help</a></li>
          <li><a href="/contact">Contact Us</a></li>
          <li><a href="/user">My Items</a></li>
        </ul>

      </div>
      <div class="col-md-3">
        <h3>CATEGORIES</h3>
        <ul class="footerList">
          <li><a href="/home/1">AGRICULTURE &#38; FOOD</a></li>
          <li><a href="/home/2">ANIMAL &#38; PET</a></li>
          <li><a href="/home/3">COMPUTER &#38; ACCESSORIES</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h3>ABOUT EYEQUEST</h3>
        <ul class="footerList">
          <li><a href="/about-us">Company Information</a></li>
          <li><a href="/copyright">Copyright</a></li>
        </ul>

      </div>
      <div class="col-md-3">
        <h3>GET UPDATES</h3>
        <form class="form-inline" action="/subscribe" method="post">
          {{ csrf_field() }}

          <input type="text" name="email" class="form-control" required="true" style="padding: 5px;"> &nbsp;

          <button type="button" class="btn btn-default submit" name="submit">Submit</button>
        </form>

      </div>

    </div>
  </div>

</div>

@include('layouts.footerAc')
