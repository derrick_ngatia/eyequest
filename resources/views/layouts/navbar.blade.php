<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <div class="row">
        <ul class="toggleForm">
          <li class="list01">
            <form class="" action="/search" method="post" onsubmit="return validate2();">
              {{ csrf_field() }}
                <input type="text" class="searchFm" placeholder="Search" name="search" id="search">
                <div class="searchHidden">
                    <input type="radio" name="srch" value="1" id="option"> Search trade or lost and found <br>
                    <input type="radio" name="srch" value="2" id="option" checked="true"> User<br>
                    <button type="submit" class="btn btn-default search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                </div>
                <div class="btn btn-default search dropSearch"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>

            </form>
          </li>
          <li >
            <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only"></span>
              <span style="color:#fff;"><strong>MENU</strong></span>
            </button>
          </li>
        </ul>
      </div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="/profession"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> HOME <span class="sr-only">(current)</span></a></li>
        <li class=""><a href="/categories"> TRADE & LOST AND FOUND </a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">BIDS <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/post/add">ADD</a></li>
            @if(isset(Auth::User()->name))
              @if(Auth::User()->userType == 1)
                <li><a href="/admin/manage">MANAGE ITEMS</a></li>
              @endif
            @endif
            <li><a href="/user">MY ITEMS</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="/user/bids">MY BIDS</a></li>
            <li><a href="/user/transactions">MY TRANSACTIONS</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">LOST & FOUND <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/lost-and-found/add">ADD</a></li>
            <li><a href="/lost-and-found">VIEW</a></li>
          </ul>
        </li>
        @if(isset(Auth::User()->userType))
          @if(Auth::User()->userType == 1)
            <li><a href="/payments">CONFIRM PAYMENTS</a></li>
          @endif
        @endif
        <li><a href="/contact"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>CONTACT US</a></li>


      </ul>

      <ul class="nav navbar-nav navbar-right">
        <form class="navbar-form navbar-left mainForm" action="/search" method="post" onsubmit="return validate();">

          {{ csrf_field() }}
          <div class="form-group">
            <input type="text" class="form-control navform" name="search" placeholder="Search" id="search1">
          </div>
          <div class="form-group ">
             <input type="radio" name="srch" value="1" class="form-control navform" id="option1"><span class="srchFm">Trade/Lost &Found </span>  <br>
          </div>
          <div class="form-group">
              <input type="radio" name="srch" value="2" class="form-control navform" id="option1" checked="true"><span class="srchFm">User </span>  <br>
          </div>
          <button type="submit" class="btn btn-default search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
        </form>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
