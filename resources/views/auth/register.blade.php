@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register
                  <div class="pull-right">
                    <a href="/login">Login here</a>
                  </div>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        @if(Session::has('messageReg'))
                          {!! Session::get('messageReg') !!}
                        @endif



                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="What's your name?">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required placeholder="Phone Number">

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Location</label>

                            <div class="col-md-6">
                              <select class="form-control" name="location" value="{{ old('location') }}" required >
                                <option value="Baringo County">Baringo County</option>
                                <option value="Bomet County">Bomet County</option>
                                <option value="Bungoma County">Bungoma County</option>
                                <option value="Busia County">Busia County</option>
                                <option value="Elgeyo Marakwet County">Elgeyo Marakwet County</option>
                                <option value="Embu County">Embu County</option>
                                <option value="Garissa County">Garissa County</option>
                                <option value="Homa Bay County">Homa Bay County</option>
                                <option value="Isiolo County">Isiolo County</option>
                                <option value="Kajiado County">Kajiado County</option>
                                <option value="Kakamega County">Kakamega County</option>
                                <option value="Kericho County">Kericho County</option>
                                <option value="Kiambu County">Kiambu County</option>
                                <option value="Kilifi County">Kilifi County</option>
                                <option value="Kirinyaga County">Kirinyaga County</option>
                                <option value="Kisii County">Kisii County</option>
                                <option value="Kisumu County">Kisumu County</option>
                                <option value="Kitui County">Kitui County</option>
                                <option value="Kwale County">Kwale County</option>
                                <option value="Laikipia County">Laikipia County</option>
                                <option value="Lamu County">Lamu County</option>
                                <option value="Machakos County">Machakos County</option>
                                <option value="Makueni County">Makueni County</option>
                                <option value="Mandera County">Mandera County</option>
                                <option value="Meru County">Meru County</option>
                                <option value="Migori County">Migori County</option>
                                <option value="Marsabit County">Marsabit County</option>
                                <option value="Mombasa County">Mombasa County</option>
                                <option value="Muranga County">Muranga County</option>
                                <option value="Nairobi County">Nairobi County</option>
                                <option value="Nakuru County">Nakuru County</option>
                                <option value="Nandi County">Nandi County</option>
                                <option value="Narok County">Narok County</option>
                                <option value="Nyamira County">Nyamira County</option>
                                <option value="Nyandarua County">Nyandarua County</option>
                                <option value="Nyeri County">Nyeri County</option>
                                <option value="Samburu County">Samburu County</option>
                                <option value="Siaya County">Siaya County</option>
                                <option value="Taita Taveta County">Taita Taveta County</option>
                                <option value="Tana River County">Tana River County</option>
                                <option value="Tharaka Nithi County">Tharaka Nithi County</option>
                                <option value="Trans Nzoia County">Trans Nzoia County</option>
                                <option value="Turkana County">Turkana County</option>
                                <option value="Uasin Gishu County">Uasin Gishu County</option>
                                <option value="Vihiga County">Vihiga County</option>
                                <option value="Wajir County">Wajir County</option>
                                <option value="West Pokot County">West Pokot County</option>
                            </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('recommend') ? ' has-error' : '' }}">
                            <label for="recommend" class="col-md-4 control-label">Recommended by E-Mail Address <small>(Optional)</small></label>

                            <div class="col-md-6">
                                <input id="recommend" type="text" class="form-control" name="recommend" value="{{ old('recommend') }}" placeholder="Who recommended you?">

                                @if ($errors->has('recommend'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('recommend') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <!-- fake fields are a workaround for chrome autofill getting the wrong fields -->
                  			<input style="display:none" type="text" name="fakeusernameremembered"/>
                  			<input style="display:none" type="password" name="password01"/>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required autocomplete="off" placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="off" required placeholder="Confirm password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary submit">
                                    Register
                                </button>
                                <button type="reset" class="btn btn-default">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
