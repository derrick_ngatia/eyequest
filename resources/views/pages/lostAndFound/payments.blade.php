@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong> </strong> </h5>
      @if(Session::has('message'))
      <p class="errors"><strong>{{ Session::get('message') }}</strong></p>
      @endif
      <form class="form-horizontal" role="form" method="POST" action="/lost-and-found/add" enctype="multipart/form-data">
        @if(Session::has('message'))
          {!! Session::get('message') !!}
        @endif
          {{ csrf_field() }}

          <div class="form-group{{ $errors->has('product_name') ? ' has-error' : '' }}">
              <label for="product_name" class="col-md-4 control-label">Owner's Name</label>

              <div class="col-md-6">
                <small> e.g (Owner's name) </small>
                  <input id="product_name" type="text" class="form-control" name="product_name" value="{{ old('product_name') }}" required autofocus>

                  @if ($errors->has('product_name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('product_name') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
              <label for="description" class="col-md-4 control-label">Description</label>

              <div class="col-md-6">
                  <small>e.g(id, certificates or any other important document )</small>
                  <textarea id="description" type="text" rows="2" class="form-control" name="description" value="" required>{{ old('description') }}</textarea>

                  @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
                  @endif
              </div>
          </div>


          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="image" class="col-md-4 control-label">Item Image <small>(optional)</small></label>

              <div class="col-md-6">
                  <input id="image" type="file" class="" name="image" value="{{ old('image') }}">

                  @if ($errors->has('image'))
                      <span class="help-block">
                          <strong>{{ $errors->first('image') }}</strong>
                      </span>
                  @endif
              </div>
          </div>


          <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                  <button type="submit" class="btn btn-primary">
                      Add Lost &amp; Found
                  </button>
                  <button type="reset" class="btn btn-default">Reset Fields</button>
              </div>
          </div>
      </form>
    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
