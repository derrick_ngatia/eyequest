@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong> LOST AND FOUND </strong> </h5>
      <div class="row">
        @forelse($posts as $post)
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6 ">
              @if($post->image != null)
                <img src="{{ url('/Eyequest/public/uploads/losts/'. $post->image )  }}" alt="{{ $post->description }}" class="thumbnail imgPost" />
              @else
                <img src="{{ url('images/logo/eyquest.jpg')  }}" alt="{{ $post->description }}"  class="thumbnail imgPost"/>
              @endif
            </div>
            <div class="col-md-6">
              <div class="panel panel-default ">
                <div class="panel-body">
                  <div class="postName">{{ $post->product_name }}</div> <br>
                  <div class="postDes"><strong>Post ID: </strong>{{ $post->productId }}<br></div>
                  <div class="postDes"><strong>Posted On: </strong> {{ $post->created_at }}<br></div>
                  <div class="postDes"><strong>Location: </strong> {{ $post->location }} <br></div>
                  <div class="postDes"><strong>Phone: </strong>
                    @if($payments != null)
                        @if($payments->confirmed == 1)
                          <span class="confirmed">{{ $post->user->phone }}</span>
                        @elseif($payments->failed == 1)
                          <span class="invalid">Payments Invalid</span>
                        @elseif($payments->confirmed == 0 && $payments->failed == 0)
                          <span class="waiting">Payments Waiting confirmation</span>
                        @endif
                      @else
                        <span class="invalid">A fee of Ksh 250 required</span>
                      @endif

                    <br></div>
                  <div class="postDes"><strong>Description: </strong> {{ $post->description }} <br></div>
                  <div class="postDes"><strong>Status:  </strong> Open<br></div>

                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-11">
              @if($payments != null)
                @if($payments->confirmed == 1)
                  <span class="confirmed">{{ $post->user->phone }}</span>
                @elseif($payments->failed == 1)
                  <span class="invalid">Payments Invalid</span>
                @elseif($payments->confirmed == 0 && $payments->failed == 0)
                  <span class="waiting">Payments Waiting confirmation, As soon as your payment is validate follow up details will be available</span>
                @else
                  <div class="direction">
                    No payments made for this post yet
                    <a href="/lost-and-found/pay/{{$post->id}}">PAY HERE</a>
                  </div>
                @endif
              @else
              <div class="direction">
                No payments made for this post yet
                <a href="/lost-and-found/pay/{{$post->id}}">PAY HERE</a>
              </div>
              @endif

            </div>
          </div>
        </div>
        @empty

        @endforelse

      </div>
    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
