@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong> LOST AND FOUND </strong> </h5>
      <div class="lost">
        @forelse($losts as $post)
        <div class="col-md-4">
          <div class="post-wrapper">
            <div class="image-description-wrapper">
              <div class="post-image-wrapper">
                  @if(isset($post->image))
                    <img src="{{ url('/Eyequest/public/uploads/losts/'. $post->image )  }}" alt="{{ $post->description }}" class="post-img" />
                  @else
                    <img src="{{ url('images/logo/eyquest.jpg')  }}" alt="{{ $post->description }}" class="post-img" />
                  @endif
              </div>
              <div class="post-description">
                <h2 class="heading"><strong> {{ $post->product_name }}</strong></h2>
                <p>{{ $post->description }}</p>
              </div>

            </div>

              <div class="links-wrapper">
              <a href="/lost/{{ $post->id }}"><i class="fa fa-plus-square" aria-hidden="true"></i> CLAIM</a>

              </div>


          </div> <!-- post-wrapper -->
        </div> <!-- col-md-4 -->
        @empty
           <p> No Posts yet </p>
        @endforelse
        <div class="pagination">
            {{ $losts->links() }}
        </div>

      </div>


    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
