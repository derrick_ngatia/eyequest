@extends('layouts.master')

@section('content')
		<style>
		      #map {
		        height: 100%;
		        min-height: 400px;
		      }
   	      </style>
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong> CONTACT US </strong> </h5>
      <div class="row">
        <div class="col-md-11">
            <h6 class="heading"><strong>LOCATION </strong></h6>
            <div class="panel panel-default">
              <div class="panel-body" id="map">
              
              	<script>
		      var map;
		      function initMap() {
		        map = new google.maps.Map(document.getElementById('map'), {
		          center: {lat: -0.285062, lng: 36.057774 },
		          zoom: 14,
		          draggable: false
		        });
		
		        var marker = new google.maps.Marker({
		          position: {lat: -0.285062, lng: 36.057774 },
		          map: map
		        });
		      }
		    </script>
		    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfXMw7PZQP14KoQnE5bBAclUq7AqvbQGU&callback=initMap"
		    async defer></script>
              </div>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-11">
            <h6 class="heading"><strong>CONTACT INFO </strong></h6>
            <div class="panel panel-default">
              <div class="panel-body">
                <address>
  	    					<p>EyeQuest.</p>
    							<p>Upper Industrial Area, Printing Press Road</p>
    							<p> P.O Box 18100-20100 Nakuru</p>
    							<p>Mobile: (+254) 704 176 341</p>
    							<p>Email: info@eyequest.co.ke</p>
  	    				</address>
              </div>
            </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-11">
          <h6 class="heading"><strong>SOCIAL MEDIA </strong></h6>
          <div class="panel panel-default">
            <div class="panel-body">
              <ul class="listsContact">
								<li>
									<a href="https://www.facebook.com/search/top/?q=eyequest%20kenya"><i class="fa fa-facebook fa-5x"></i></a>
								</li>
								<li>
									<a href="https://twitter.com/eyequestkenya"><i class="fa fa-twitter fa-5x"></i></a>
								</li>
								<li>
									<a href="https://www.instagram.com/eyequestkenya/"><i class="fa fa-instagram fa-5x"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-youtube fa-5x"></i></a>
								</li>
							</ul>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-11">
          <h6 class="heading" draggable="true"><strong>TALK TO US </strong></h6>
        <div class="panel panel-default">
          <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="/contact" >
              @if(Session::has('message'))
                {!! Session::get('message') !!}
              @endif
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Name </label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">Email </label>

                    <div class="col-md-6">
                        <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Your email" required autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                    <label for="subject" class="col-md-4 control-label">Subject </label>

                    <div class="col-md-6">
                        <input id="subject" type="text" class="form-control" placeholder="Subject" name="subject" value="{{ old('subject') }}" required autofocus>

                        @if ($errors->has('subject'))
                            <span class="help-block">
                                <strong>{{ $errors->first('subject') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                    <label for="message" class="col-md-4 control-label"> Message </label>

                    <div class="col-md-6">
                        <textarea id="message" type="text" rows="3" class="form-control" name="message" value="" placeholder="Message" required>{{ old('message') }}</textarea>

                        @if ($errors->has('message'))
                            <span class="help-block">
                                <strong>{{ $errors->first('message') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary submit">
                            Submit
                        </button>
                        <button type="reset" class="btn btn-default">Reset Fields</button>
                    </div>
                </div>
            </form>
          </div>
        </div>

        </div>

      </div>



    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
