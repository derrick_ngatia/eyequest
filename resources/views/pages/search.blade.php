@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong>SEARCH RESULTS</strong> </h5>
      <div class="row">
        @forelse ($posts as $post)
        @if($post->postCategory == 1)
            <div class="col-md-4">
              <div class="post-wrapper">
                <div class="image-description-wrapper">
                  <div class="slide-content">
                    <h2 class="heading"> {{ $post->product_name }}</h2> <br>
                    <strong>Description: </strong>{{ $post->description }} <br>
                    <strong>Location: </strong>{{ $post->location }} <br>
                    <strong>BID WITH: </strong>{{ $post->bid_with }} <br>
                    <strong>Status: </strong> Open <br>
                    <a href="/Bid-item/{{ $post->id }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <strong>BID NOW</strong> </a>
                  </div>
                  <div class="static-content">
                    <div class="post-image-wrapper">
                        @if(isset($post->image))
                          <img src="{{ url('/Eyequest/public/uploads/posts/'. $post->image )  }}" alt="{{ $post->description }}" class="post-img" />
                        @else
                          <img src="{{ url('images/logo/eyquest.jpg')  }}" alt="{{ $post->description }}" class="post-img" />
                        @endif
                    </div>
                    <div class="post-description">
                      <h2 class="heading"><strong> {{ $post->product_name }}</strong></h2>
                      <p>{{ $post->description }}</p>
                    <div class="shopping-cart">
                      <i class="fa fa-shopping-cart" aria-hidden="true"></i> ({{ $post->bidders }})Bid
                    </div>
                    </div>
                  </div>
                </div>
                  <div class="links-wrapper">
                    <a href="/Suggestions/{{ $post->id}}"><i class="fa fa-plus-square" aria-hidden="true"></i> suggestions({{ $post->suggestions }}) </a>  &nbsp;
                    <a href="/Bidders/{{ $post->id}}"><i class="fa fa-plus-square" aria-hidden="true"></i> Bidders({{ $post->bidders }})</a>
                  </div>
              </div> <!-- post-wrapper -->
            </div> <!-- col-md-4 -->
            @else
            <div class="col-md-4">
              <div class="post-wrapper">
                <div class="image-description-wrapper">
                  <div class="post-image-wrapper">
                      @if(isset($post->image))
                        <img src="{{ url('/Eyequest/public/uploads/losts/'. $post->image )  }}" alt="{{ $post->description }}" class="post-img" />
                      @else
                        <img src="{{ url('images/logo/eyquest.jpg')  }}" alt="{{ $post->description }}" class="post-img" />
                      @endif
                  </div>
                  <div class="post-description">
                    <h2 class="heading"><strong> {{ $post->product_name }}</strong></h2>
                    <p>{{ $post->description }}</p>
                  </div>

                </div>

                  <div class="links-wrapper">
                  <a href="/lost/{{ $post->id }}"><i class="fa fa-plus-square" aria-hidden="true"></i> CLAIM</a>

                  </div>


              </div> <!-- post-wrapper -->
            </div> <!-- col-md-4 -->
            @endif
            @empty
            <div class="col-md-4">
              <p>No search results found</p>
            </div>
            @endforelse
        </div>
        <div class="pagination">
          
        </div>
    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
