@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong> View Bidders </strong> </h5>
      <div class="row">
        @forelse($posts as $post)
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6 ">
                @if($post->image != null)
                  <img src="{{ url('/Eyequest/public/uploads/posts/'. $post->image )  }}" alt="{{ $post->description }}" class="thumbnail imgPost" />
                @else
                  <img src="{{ url('images/logo/eyquest.jpg')  }}" alt="{{ $post->description }}"  class="thumbnail imgPost"/>
                @endif
            </div>
            <div class="col-md-6">
              <div class="panel panel-default ">
                <div class="panel-body">
                  <div class="postName">{{ $post->product_name }}</div> <br>
                  <div class="postDes"><strong>Post ID: </strong>{{ $post->productId }}<br></div>
                  <div class="postDes"><strong>Posted On: </strong> {{ $post->created_at }}<br></div>
                  <div class="postDes"><strong>Location: </strong> {{ $post->location }} <br></div>
                  <div class="postDes"><strong>Description: </strong> {{ $post->description }} <br></div>
                  <div class="postDes"><strong>Status:  </strong> Open<br></div>
                  <div class="postDes"><a href="/Bid-item/{{ $post->id }}" class="btn btn-default submit">Make Bid</a> <a href="/suggest-item/{{ $post->id }}" class="btn btn-default submit">Make Suggestion</a></div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              @if($post->user_id == Auth::User()->id)

                @forelse($post->bids as $bid)
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong>Bidder Name:</strong> {{ $bid->user->name }} &nbsp; <strong>Bidder Number:</strong>{{ $bid->user->phone }}
                  </div>
                  <div class="panel-body">
                    <div class="col-md-6">
                      @if(isset($post->image))
                        <img src="{{ url('/Eyequest/public/uploads/bids/'. $bid->image )  }}" alt="{{ $post->description }}" class="post-img thumbnail" />
                      @else
                        <img src="{{ url('images/logo/eyquest.jpg')  }}" alt="{{ $post->description }}" class="post-img thumbnail" />
                      @endif
                    </div>
                    <div class="col-md-6">
                      <h5><strong>Other details </strong></h5>
                      <strong>Location:</strong> {{ $bid->user->location }} <br>
                      <strong>Offering:</strong> {{ $bid->bid_with }}
                    </div>
                  </div>

                </div>

                @empty
                  <div class="panel panel-default">
                    <div class="panel-heading">
                        No bidders yet
                    </div>
                  </div>
                @endforelse
              @else
                  @if($post->confirmed == 1)
                    @forelse($post->bids as $bid)
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <strong>Bidder Name:</strong> {{ $bid->user->name }} &nbsp; <strong>Bidder Number:</strong>{{ $bid->user->phone }}
                      </div>
                      <div class="panel-body">
                          <h6>Other details </h6>
                          Location: {{ $bid->user->location }} <br>
                          Offering: {{ $bid->bid_with }}
                      </div>

                    </div>

                    @empty
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            No bidders yet
                        </div>
                      </div>
                    @endforelse
                  @esleif($post->failed == 1)
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <span class="invalid">Payments Invalid, The payment you made was not valid <br> Please reconfirm your details </span>
                    </div>
                    <div class="panel-body">
                      <div class="links-wrapper">
                        <a href="/confirm-lost/{{ $post->id }}"><i class="fa fa-plus-square" aria-hidden="true"></i> Re-Confirm Payments </a>
                      </div>
                    </div>
                  </div>
                  @elseif($post->confirmed == 0 && $post->filed == 0)
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <span class="waiting">Waiting Confirmation <br>
                         Your payments is waiting to be  confirmed <br>
                         As soon as your payment is confirmed you will be able to view the phone to follow your details </span>
                    </div>
                    <div class="panel-body">
                      <div class="links-wrapper">
                        <a href="/confirm-lost/{{ $post->id }}"><i class="fa fa-plus-square" aria-hidden="true"></i> Re-Confirm Payments </a>
                      </div>
                    </div>
                  </div>
                  @endif

              @endif
            </div>
          </div>
        </div>

        @empty
            No content to display
        @endforelse




      </div>

    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
