@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong>CREATE POST</strong> </h5>
      <form class="form-horizontal" role="form" method="POST" action="/create/add" enctype="multipart/form-data">
        @if(Session::has('message'))
          {!! Session::get('message') !!}
        @endif
          {{ csrf_field() }}

          <div class="form-group{{ $errors->has('product_name') ? ' has-error' : '' }}">
              <label for="product_name" class="col-md-4 control-label">Product Name</label>

              <div class="col-md-6">
                  <input id="product_name" type="text" class="form-control" name="product_name" value="{{ old('product_name') }}" required autofocus>

                  @if ($errors->has('product_name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('product_name') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group{{ $errors->has('bid_with') ? ' has-error' : '' }}">
              <label for="bid_with" class="col-md-4 control-label"> Exchange With </label>

              <div class="col-md-6">
                  <input id="bid_with" type="text" class="form-control" name="bid_with" value="{{ old('bid_with') }}" required>

                  @if ($errors->has('bid_with'))
                      <span class="help-block">
                          <strong>{{ $errors->first('bid_with') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
              <label for="description" class="col-md-4 control-label">Product Description</label>

              <div class="col-md-6">
                  <textarea id="description" type="text" rows="4" class="form-control" name="description" value="" required>{{ old('description') }}</textarea>

                  @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
              <label for="location" class="col-md-4 control-label">Location </label>

              <div class="col-md-6">
                <select class="form-control" name="location" value="{{ old('location') }}" required >
                  <option value="Baringo County">Baringo County</option>
                  <option value="Bomet County">Bomet County</option>
                  <option value="Bungoma County">Bungoma County</option>
                  <option value="Busia County">Busia County</option>
                  <option value="Elgeyo Marakwet County">Elgeyo Marakwet County</option>
                  <option value="Embu County">Embu County</option>
                  <option value="Garissa County">Garissa County</option>
                  <option value="Homa Bay County">Homa Bay County</option>
                  <option value="Isiolo County">Isiolo County</option>
                  <option value="Kajiado County">Kajiado County</option>
                  <option value="Kakamega County">Kakamega County</option>
                  <option value="Kericho County">Kericho County</option>
                  <option value="Kiambu County">Kiambu County</option>
                  <option value="Kilifi County">Kilifi County</option>
                  <option value="Kirinyaga County">Kirinyaga County</option>
                  <option value="Kisii County">Kisii County</option>
                  <option value="Kisumu County">Kisumu County</option>
                  <option value="Kitui County">Kitui County</option>
                  <option value="Kwale County">Kwale County</option>
                  <option value="Laikipia County">Laikipia County</option>
                  <option value="Lamu County">Lamu County</option>
                  <option value="Machakos County">Machakos County</option>
                  <option value="Makueni County">Makueni County</option>
                  <option value="Mandera County">Mandera County</option>
                  <option value="Meru County">Meru County</option>
                  <option value="Migori County">Migori County</option>
                  <option value="Marsabit County">Marsabit County</option>
                  <option value="Mombasa County">Mombasa County</option>
                  <option value="Muranga County">Muranga County</option>
                  <option value="Nairobi County">Nairobi County</option>
                  <option value="Nakuru County">Nakuru County</option>
                  <option value="Nandi County">Nandi County</option>
                  <option value="Narok County">Narok County</option>
                  <option value="Nyamira County">Nyamira County</option>
                  <option value="Nyandarua County">Nyandarua County</option>
                  <option value="Nyeri County">Nyeri County</option>
                  <option value="Samburu County">Samburu County</option>
                  <option value="Siaya County">Siaya County</option>
                  <option value="Taita Taveta County">Taita Taveta County</option>
                  <option value="Tana River County">Tana River County</option>
                  <option value="Tharaka Nithi County">Tharaka Nithi County</option>
                  <option value="Trans Nzoia County">Trans Nzoia County</option>
                  <option value="Turkana County">Turkana County</option>
                  <option value="Uasin Gishu County">Uasin Gishu County</option>
                  <option value="Vihiga County">Vihiga County</option>
                  <option value="Wajir County">Wajir County</option>
                  <option value="West Pokot County">West Pokot County</option>
              </select>

                  @if ($errors->has('location'))
                      <span class="help-block">
                          <strong>{{ $errors->first('location') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
              <label for="category" class="col-md-4 control-label">Select Category</label>

              <div class="col-md-6">
                <select class="form-control" name="category" value="{{ old('category') }}" required >
                  <option value="1">AGRICULTURE &#38; FOOD</option>
                  <option value="2">ANIMAL &#38; PET</option>
                  <option value="3">COMPUTER &#38; ACCESSORIES</option>
                  <option value="4">CONSTRUCTION &#38; INDUSTRY</option>
                  <option value="5">FASHION &#38; BEAUTY</option>
                  <option value="6">HOME &#38; FURNITURE</option>
                  <option value="7">INDENTIFICATION CARDS</option>
                  <option value="9">VEHICLE</option>
              </select>

                  @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="image" class="col-md-4 control-label">Product Image <small>(optional)</small></label>

              <div class="col-md-6">
                  <input id="image" type="file" class="form-control" name="image" value="{{ old('image') }}">

                  @if ($errors->has('image'))
                      <span class="help-block">
                          <strong>{{ $errors->first('image') }}</strong>
                      </span>
                  @endif
              </div>
          </div>


          <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                  <button type="submit" class="btn btn-primary">
                      Create Post
                  </button>
                  <button type="reset" class="btn btn-default">Reset</button>
              </div>
          </div>
      </form>
    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
