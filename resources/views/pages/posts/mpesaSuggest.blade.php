@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong> LIPA NA MPESA </strong> </h5>
      <div class="row">
        <div class="col-md-11 centered topPay">
            <div class="payImage ">
              <img src="{{ url('images/logo/eyepay.jpg')  }}" alt="MPESA PAYMENTS" class="payment"/>
            </div>
            <span class="main">{{ Auth::User()->name }}</span> you are required to pay <span class="main">50</span> inorder to bid for the item you selected
        </div>
        <div class="col-md-11 instruction">
          <p>MPESA INSTRUCTIONS:

           1.Access Mpesa Menu<br />
           2.Select Lipa na M-PESA (or Payment services)<br />
           3.Select Buy Goods & Services<br />
           4.Enter Till number <i style="color:#FF0000">781425</i> and select OK<br />
           5.Enter Amount <i style="color:#FF0000"> 50 </i>and select OK<br />
           6.Enter your M-Pesa pin and select OK<br />
           7.For instant/ automatic payment Confirmation, please verify the Mpesa Transaction ID below<br />

            </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-11 payForm">
          <div class="panel panel-default">
            <div class="panel-body">
              <form class="form" action="/save_suggestion" method="post" enctype="multipart/form-data">
                @if(Session::has('message'))
                  {!! Session::get('message') !!}
                @endif
                  {{ csrf_field() }}
                  <input type="hidden" name="post_id" value="{{ $post->id }}">
                  <input type="hidden" name="postCategory" value="{{ $post->category_id  }}">
                  <input type="hidden" name="amount" value="50" >
                  <div class="col-md-6">
                    <label for="refNo" class="col-md-12 control-label">Mpesa Reference <small>(Required)</small></label>
                    <div class="form-group{{ $errors->has('refNo') ? ' has-error' : '' }}">
                        <div class="">
                            <input id="refNo" type="text" class="form-control col-md-12" name="refNo" value="{{ old('refNo') }}" required autofocus>

                            @if ($errors->has('refNo'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('refNo') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <label for="number" class="col-md-12 control-label"> Mpesa Number <small>(Required)</small> </label>
                    <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                        <div class="">
                            <input id="number" type="text" class="form-control col-md-12" name="number" value="{{ old('number') }}" required>

                            @if ($errors->has('number'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('number') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <label for="suggestion" class="col-md-12 control-label">Give a Suggestion: <small>(Required)(Filled with owners preference)</small></label>
                    <div class="form-group{{ $errors->has('suggestion') ? ' has-error' : '' }}">
                        <div class="">
                            <input id="suggestion" type="text" class="form-control col-md-12" name="suggestion" value="{{ $post->bid_with }}" required autofocus>

                            @if ($errors->has('suggestion'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('suggestion') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label for="description" class="col-md-12 control-label"> Description <small>(Required)</small> </label>
                    <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                        <div class="">
                            <textarea id="description" type="text" class="form-control col-md-12" rows="2" name="description" value="" required>{{ old('description') }}</textarea>

                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label for="image" class="col-md-12 control-label"> Select an image  </label>
                    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                        <div class="">
                            <input id="image" type="file" class="form-control col-md-12" name="image" value="{{ old('image') }}">

                            @if ($errors->has('image'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                  </div>

                  <div class="col-md-12 submitButton">
                    <button type="submit" class="btn btn-primary submit">Submit</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                  </div>
                  </div>
                </form>
            </div>

          </div>

        </div>

      </div>

    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
