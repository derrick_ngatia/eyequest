@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong> View Suggestions </strong> </h5>
      <div class="row">
        @forelse($posts as $post)
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6 ">
                @if($post->image != null)
                  <img src="{{ url('/Eyequest/public/uploads/posts/'. $post->image )  }}" alt="{{ $post->description }}" class="thumbnail imgPost" />
                @else
                  <img src="{{ url('images/logo/eyquest.jpg')  }}" alt="{{ $post->description }}"  class="thumbnail imgPost"/>
                @endif
            </div>
            <div class="col-md-6">
              <div class="panel panel-default ">
                <div class="panel-body">
                  <div class="postName">{{ $post->product_name }}</div> <br>
                  <div class="postDes"><strong>Post ID: </strong>{{ $post->productId }}<br></div>
                  <div class="postDes"><strong>Posted On: </strong> {{ $post->created_at }}<br></div>
                  <div class="postDes"><strong>Location: </strong> {{ $post->location }} <br></div>
                  <div class="postDes"><strong>Owner's Contact: </strong> {{ $post->location }} <br></div>


                  <div class="postDes"><strong>Description: </strong> {{ $post->description }} <br></div>
                  <div class="postDes"><strong>Status:  </strong> Open<br></div>
                  <div class="postDes"><a href="/Bid-item/{{ $post->id }}" class="btn btn-default submit">Make Bid</a> <a href="/suggest-item/{{ $post->id }}" class="btn btn-default submit">Make Suggestion</a></div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <h5 class="heading">Suggestions</h5>
              @forelse($post->suggestion as $bid)
              <div class="panel panel-default">
                <div class="panel-heading">
                  <strong>Name:</strong> {{ $bid->user->name }} &nbsp; <strong>Number:</strong>{{ $bid->user->phone }}
                </div>
                <div class="panel-body">
                  <div class="col-md-6">
                    @if(isset($post->image))
                      <img src="{{ url('/Eyequest/public/uploads/suggestions/'. $bid->image )  }}" alt="{{ $post->description }}" class="post-img thumbnail" />
                    @else
                      <img src="{{ url('images/logo/eyquest.jpg')  }}" alt="{{ $post->description }}" class="post-img thumbnail" />
                    @endif
                  </div>
                  <div class="col-md-6">
                    <h5><strong> Other details </strong></h5>
                    <strong>Location:</strong> {{ $bid->user->location }} <br>
                    <strong>Offering:</strong> {{ $bid->suggestion }}
                  </div>

                </div>
              </div>
              @empty
                <div class="panel panel-default">
                  <div class="panel-heading">
                      No bidders yet
                  </div>
                </div>
              @endforelse
            </div>
          </div>
        </div>
        @empty
            No content to display
        @endforelse
      </div>
    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
