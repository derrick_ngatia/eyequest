@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong> USERS FOUND </strong> </h5>
      <div class="row">
        <div class="col-md-11">
          @forelse($posts as $user)
          <div class="col-md-6">
            <div class="media srchMedia">
              <div class="media-body">
              <h4 class="media-heading"><a href="/UserProfile/{{ $user->name }}/{{ $user->id }}" class="srchLink"> {{ $user->name }} </a> </h4>
              <p> <strong>Profession: </strong>{{ $user->talent }}</p>
              <p> <strong>Points: </strong> {{ $user->points }} </p>
              </div>
            </div>
          </div>
          @empty
            No user found
          @endforelse

        </div>
      </div>


    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
