@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-9">
      <h5 class="heading"> <strong> EMAIL CONFIRMATION </strong> </h5>
      <div class="row">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>HELLO {{ Auth::User()->name }}</strong>
          </div>
          <div class="panel-body">
            @if(Session::has('sent'))
              {!! Session::get('sent') !!}
            @endif
            You are a step away, We sent an email to {{ Auth::User()->email }}.
            Please Login to your account to confirm your email address
          </div>
          <div class="panel-footer">
            If you never received the email click the button below to resend
            <br>
            <a href="/resendConfirmation" class="btn btn-default submit">Resend Email</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
