<div class="categoryHeader">
  <h5 class="heading"> <strong> <a href="#">SHOW CATEGORIES</a></strong> </h5>
</div>
<div class="category">
<h5 class="heading"> <strong>CATEGORIES</strong> </h5>
<div class="row">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="category">
        <a href="/home/1">AGRICULTURE &#38; FOOD</a>
      </div>
      <div class="category">
        <a href="/home/2">ANIMAL &#38; PET</a>
      </div>
      <div class="category">
        <a href="/home/3">COMPUTER &#38; ACCESSORIES</a>
      </div>
      <div class="category">
        <a href="/home/4">CONSTRUCTION &#38; INDUSTRY</a>
      </div>
      <div class="category">
        <a href="/home/5">FASHION &#38; BEAUTY</a>
      </div>
      <div class="category">
        <a href="/home/6">HOME &#38; FURNITURE</a>
      </div>
      <div class="category">
        <a href="/home/7">IDENTIFICATION CARDS</a>
      </div>
      <div class="category">
        <a href="/home/9">VEHICLE</a>
      </div>
    </div>
  </div>
</div>
</div>

<div class="row">
  <div class="panel panel-default categoryMain">
    <div class="panel-body eyequestImg">
        <img src="{{ url('/Eyequest/public/uploads/admin/admin.jpeg')  }}" alt="Eye-Quest" class="eyequestImg" />
    </div>
  </div>
</div>
