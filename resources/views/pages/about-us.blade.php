@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong> ABOUT US </strong> </h5>
      <div class="row">
        <div class="col-md-11">
          <p>EyeQuest is an online business environment that offers  the ultimate platform where users are able to display their skills, products, look for jobs and  professional profiling  besides learning from each other ,hence leading to general improvement of their skills.it also provides a platform for  trade( both barter and monetary ), online shopping, services  promotion and  a lost -and-found section. It basically allows an interface for our users, client and subscribers to create adverts and display them on social network, thus enabling them to reach out to the market. Simply it’s THE ULTIMATE HUSTLERS CORNER.</p> <br>

          <strong>FUNCTIONALITY</strong> <br>
          <strong>Profession /skills/ product</strong><br>
          <p><i>-users will be able to create accounts and post items. The posted items will basically revolve around their products, skills or
             professions. Once posted it will be shared with other subscribers the user has linked to. </i></p>

          <p> <i>-upon account creation they will have to create links with people in the same field of profession
            which in turn will allow them to share their work  hence learn from each other.</i> </p>

            <p><i>-linked user will have the ability to ask pressing questions relating to their (user’s) professions, to all the
              members they have linked with. On answering the question points will be awarded.</i></p>

          <p><i>-The points awarded help in marketing a given account holder, where those with the highest points will always have a higher
             priority than those with less points within a given search. Users will also earn points from how
            many recommendations they get per post and how many approvals they get from the answers they give to questions posted on site.</i></p>

          <p><i>-Users will also have to update their Employment status every once a year, or any time there status changes.
             This will enable them to get jobs easily .eg whenever  an employer is looking for a given skill  they will be able to get a
             list according to priority and location, then invite them for interviews knowing exactly their  employment status  having had
             a look at thier employment profile .</i></p> <br>

          <strong>Trade{cash and barter}</strong><br>

          a.	Users register with us <br>
          b.	They upload the items with the description of what they want for
              the item.<br>
          c.	The item is displayed on the main site.<br>
          d.	Then members can start biding against the item by uploading what
              they have.<br>

          e.  posting an item on the site is free.<br>
          f.  When biding for an item on the site you will be required to pay
              50/- so as to post a bid against the item.<br>
          g.  When biding for a suggestion on the site you will be charged 50/-. <br>
          h.  If the trader is interested in the item that biders have
              displayed then he/she can click the done or ok button to remove the item from the biding site. <br>
          i.  Then they can be given instructions and contacts to get their items.<br>
          j.  When posting items, one should give his contacts and google
              directions but they will not be displayed until the transaction is done.<br>
          h.  The other items that biders have uploaded for biding against a
              certain item is open for anybody to bid against  only if the transaction
              for the item they have bid against has not been closed or been selected.<br>
              Note: item is displayed with its description only. <br><br>

              <strong>Lost and found.</strong><br> 
            A platform where users post item they found  and their owners are not
            known. You upload with the description and location and when the owners find them they pay 250/- to get item directions and contact of the person who posted. Out of the 250/, -150 /- will be credited to the person who posted the item
            -EyeQuest allows individual to post items for cash, where one is able to sell his or her item.






        </div>
      </div>


    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
