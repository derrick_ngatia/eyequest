@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong>PROFESSION, SKILLS & PRODUCTS </strong> </h5>
      <div class="row cvMain">
        <div class="col-md-8 ">
          <div class="col-md-12 topLinksTalents">
            <a class="btn btn default submit" id="cvFormDrop">Post</a>
            <a class="btn btn-default submit pull-right" id="cdQ">Ask question</a>
          </div>
          <br><br>
          <div class="col-md-12 cvForm">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/talents') }}" enctype="multipart/form-data">
              @if(Session::has('message'))
                {!! Session::get('message') !!}
              @endif
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description" class="col-md-4 control-label">Post Description</label>

                    <div class="col-md-8">
                        <textarea id="description" type="text" rows="2" class="form-control" name="description" value="" required>{{ old('description') }}</textarea>
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('product_name') ? ' has-error' : '' }}">
                  <label for="description" class="col-md-4 control-label"></label>
                  <div class="col-md-8">
                    <span class="btn btn-default btn-file">
                      Post Image <input type="file" name="image" class="">
                    </span>
                  </div>

                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary submit">
                            Create Post
                        </button>
                        <button type="reset" class="btn btn-default">Reset Fields</button>
                    </div>
                </div>
            </form>
          </div>

          <div class="QuestionForm">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/talents/question') }}" enctype="multipart/form-data">
              @if(Session::has('message'))
                {!! Session::get('message') !!}
              @endif
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description" class="col-md-4 control-label">Question Description</label>

                    <div class="col-md-8">

                        <textarea id="description" type="text" rows="2" class="form-control" name="description" value="" required>{{ old('description') }}</textarea>

                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('product_name') ? ' has-error' : '' }}">
                  <label for="description" class="col-md-4 control-label"></label>
                  <div class="col-md-8">
                    <span class="btn btn-default btn-file">
                      Post Image(optional) <input type="file" name="image" class="">
                    </span>
                  </div>

                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary submit">
                            Create Post
                        </button>
                        <button type="reset" class="btn btn-default">Reset Fields</button>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
        <div class="col-md-8">
          @forelse($posts as $post)
            <div class="talentMain">
              <div class="postOwn">
                @if(isset($post->user->avatar))
                  <img src="{{ url('/Eyequest/public/uploads/avatars/'.$post->user->avatar)  }}" alt="Eye-Quest" class="OwnAvatar">
                  <!--<img src="{{ public_path('uploads/avatars/'.$post->user->avatar) }}" alt="Eye-Quest" class="OwnAvatar">-->
                @else
                  <img src="{{ url('images/logo/eyquest.jpg')  }}" alt="Eye-Quest" class="OwnAvatar">
                @endif
                <strong><a href="/UserProfile/{{$post->user->name .'/'. $post->user->id  }}" class="repLink">{{ $post->user->name }}</a>
                    @if($post->profileupdatepost == 1)
                      @if(isset($post->user->talent))
                        
                          ({{ $post->user->talent }}) Just Joined EyeQuest
                        
                      @endif
                    @endif
                </strong>

              </div>
              <div class="postImg">
                @if(isset($post->image))
                  <img src="{{ url('/Eyequest/public/uploads/cvposts/'.$post->image)  }}" alt="" class="talentImg">
                @endif
              </div>
              <br>
              {{ $post->description }}
              <br>
              @if($post->profileupdatepost == 1)
                <div class="" style="text-align:center">
                  <strong>Welcome to Eyequest {{ $post->user->name }}</strong>
                </div>
              @endif
              <div class="comments">
                @if(Auth::User()->id != $post->user->id)
                <a href="/talents/recommend/{{ $post->id }}" class="recommLink" id="viewCm"><strong>{{ count($post->recommends) }}  <i class="fa fa-bolt" aria-hidden="true"></i> Recommend</strong></a>
                @else
                <a id="viewCm" class="recommends"><strong>{{ count($post->recommends) }}  <i class="fa fa-bolt" aria-hidden="true"></i> Recommend</strong></a>
                @endif
                <a href="#" class="repLinks pull-right" id="viewCm"><strong><span class="tggl">view</span> <span class="rep"><?php echo count($post->replies);  ?></span> comments</strong></a>

                <!-- Adding this div -->
                <div class="recommendationDiv">
                  @forelse($post->recommends as $recommendation)
                  <div class="postOwn">
                    @if(isset($post->user->avatar))
                      <img src="{{ url('/Eyequest/public/uploads/avatars/'.$recommendation->user->avatar)  }}" alt="Eye-Quest" class="OwnAvatar">
                    @else
                      <img src="{{ url('images/logo/eyquest.jpg')  }}" alt="Eye-Quest" class="OwnAvatar">
                    @endif
                    <strong><a href="/UserProfile/{{$recommendation->user->name .'/'. $recommendation->user->id  }}" class="repLink">{{ $recommendation->user->name }}</a></strong>
                  </div>
                  @empty

                  @endforelse
                </div>

                <div class="recomerrors">

                </div>
                <div class="mainComm">
                  @forelse($post->replies as $reply)
                  @if($post->question == 1)
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            @if(isset($reply->user->avatar))

                            @else

                            @endif
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading">Posted by {{ $reply->user->name }}   <small>posted on: {{ $reply->created_at }}</small> </h4>
                          {{ $reply->reply}}
                        </div>

                        @if(Auth::User()->id == $post->user_id)
                          @if(Auth::User()->id != $reply->user_id)
                            <a href="/talents/approve/{{ $reply->id .'/'. $reply->user_id}}" class="approvebtn"><button type="button" name="button" class="btn btn-default submit">Approve</button></a>
                          @endif
                        @endif
                        <div class="appErrors">

                        </div>
                      </div>
                  @else
                        <div class="media">
                          <div class="media-left">
                            <a href="#">
                              @if(isset($reply->user->avatar))

                              @else

                              @endif
                            </a>
                          </div>
                          <div class="media-body">
                            <h4 class="media-heading">Posted by {{ $reply->user->name }}   <small>posted on: {{ $reply->created_at }}</small> </h4>
                            {{ $reply->reply}}
                          </div>
                        </div>
                  @endif
                  @empty

                  @endforelse
                </div>
                <div class="cmForm">
                  <form class="form-horizontal commentForm" role="form" method="POST" action="/talents/comments" enctype="multipart/form-data">
                    <div class="cmerrors">

                    </div>
                    @if(Session::has('message'))
                      {!! Session::get('message') !!}
                    @endif
                      {{ csrf_field() }}
                      <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                          <div class="col-md-11">
                              <textarea id="comment" type="text" rows="2" class="form-control" name="comment" required>{{ old('comment') }}</textarea>
                              @if ($errors->has('comment'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('comment') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                      <input type="hidden" name="post_id" class="postId" value="{{ $post->id }}">
                      <button type="submit" class="btn btn-primary submit commentBtn">
                         <i class="fa fa-comment" aria-hidden="true"></i> Comment
                      </button>
                  </form>
                </div>

                  <script type="text/javascript">
                    //Setting token
                    var token = '{{ Session::token() }}';
                    var url = '{{ route('comment') }}';
                    var user = '{{ Auth::User()->name }}';
                  </script>
              </div>
            </div>
          @empty

          @endforelse

          {{ $posts->links() }}
        </div>

    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
