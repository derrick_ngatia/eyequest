@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong>Profile</strong> </h5>
      <div class="row">
        <div class="col-md-4">
          <div class="profSection">
            <div class="profImage">
              @if(isset($user->avatar))
                <img src="{{ url('/Eyequest/public/uploads/avatars/'. $user->avatar )  }}" alt="Eye-Quest" class="eyequestImg" />
              @else
              <img src="{{ url('eyequest/icons/eyequestship.jpg') }}" alt="Eye-Quest" class="eyequestImg" />
              @endif
            </div>

            <div class="Update">
              <div class="UpdateWithin">
                <a href="#"><strong class="pull-left"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span> Barter Trade: </strong> <span class="badge pull-right">{{ count($trade) }}</span></a>
              </div>
              <div class="UpdateWithin">
                <a href="#"><strong class="pull-left"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Lost and Found: </strong> <span class="badge pull-right">{{ count($losts)}}</span></a>
              </div>
              <div class="UpdateWithin">
                <a href="#"><strong class="pull-left"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Profession: </strong> <span class="badge pull-right">{{ count($talents) }}</span></a>
              </div>
              <div class="UpdateWithin">
                <a href="#"><strong class="pull-left"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Points: </strong> <span class="badge pull-right">{{ $user->points }}</span></a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div class="media profMedia">
            <div class="pull-left">
              <strong>Name</strong>
            </div>
            <div class="media-body">
              {{ $user->name }}
            </div>
          </div>

          <div class="media profMedia">
            <div class="pull-left">
              <strong>Profession, Skills & Products </strong>
            </div>
            <div class="media-body">
              {{ $user->talent }}
            </div>
          </div>

          <div class="media profMedia">
            <div class="pull-left">
              <strong> Description </strong>
            </div>
            <div class="media-body">
              {{ $user->cv }}
            </div>
          </div>

          <div class="media profMedia">
            <div class="pull-left">
              <strong>Status: </strong>
            </div>
            <div class="media-body">
              @if($user->employed == 1)
                  <p>Employed</p>
              @else
                  <p>Unemployed</p>
              @endif
            </div>
          </div>

        </div>
      </div>


      <div class="row">
        <div class="col-md-12">
          <div class="row profileNavmain">
            <div class="profileNav">
              <ul>
                <!--<li><div class="col-md-4 profLinks"><a href="#"><strong>Barter Trade</strong></a></div></li>
                <li><div class="col-md-4 profLinks"><a href="#"><strong>Lost & Found</strong></a></div> </li> -->
                <li><div class="col-md-4 profLinks"><a href="#"><strong>Profession, Skills $ Products</strong></a></div></li>
              </ul>
            </div>

                <div class="col-md-8">

                  @forelse($talents as $post)
                    <div class="talentMain">
                      <div class="postOwn">
                        @if(isset($post->user->avatar))
                          <img src="{{url('/Eyequest/public/uploads/avatars/'.$post->user->avatar)  }}" alt="Eye-Quest" class="OwnAvatar">
                        @else
                          <img src="{{ url('images/logo/eyquest.jpg')  }}" alt="Eye-Quest" class="OwnAvatar">
                        @endif
                        <strong><a href="/UserProfile/{{$post->user->name .'/'. $post->user->id  }}" class="repLinks">{{ $post->user->name }}</a></strong>

                      </div>

                      @if(isset($post->image))
                        <img src="{{ url('/Eyequest/public/uploads/cvposts/'.$post->image)  }}" alt="" class="talentImg">
                      @endif
                      <br>
                      <br>
                      {{ $post->description }}
                      <div class="comments">
                        @if(Auth::User()->id != $post->user->id)
                        <a href="/talents/recommend/{{ $post->id }}" class="recommLink" id="viewCm"><strong>{{ count($post->recommends) }}  <i class="fa fa-bolt" aria-hidden="true"></i> Recommend</strong></a>
                        @else
                        <a id="viewCm"><strong>{{ count($post->recommends) }}  <i class="fa fa-bolt" aria-hidden="true"></i> Recommend</strong></a>
                        @endif
                        <a href="#" class="repLinks pull-right" id="viewCm"><strong><span class="tggl">view</span> <?php echo count($post->replies);  ?> comments</strong></a>
                        <div class="recomerrors">

                        </div>
                        <div class="mainComm">
                          @forelse($post->replies as $reply)
                          @if($post->question == 1)
                              <div class="media">
                                <div class="media-left">
                                  <a href="#">
                                    @if(isset($reply->user->avatar))

                                    @else

                                    @endif
                                  </a>
                                </div>
                                <div class="media-body">
                                  <h4 class="media-heading">Posted by {{ $reply->user->name }}   <small>posted on: {{ $reply->created_at }}</small> </h4>
                                  {{ $reply->reply}}
                                </div>

                                @if(Auth::User()->id == $post->user_id)
                                  @if(Auth::User()->id != $reply->user_id)
                                    <a href="/talents/approve/{{ $reply->id .'/'. $reply->user_id}}" class="approvebtn"><button type="button" name="button" class="btn btn-default submit">Approve</button></a>
                                  @endif
                                @endif
                                <div class="appErrors">

                                </div>
                              </div>
                          @else
                                <div class="media">
                                  <div class="media-left">
                                    <a href="#">
                                      @if(isset($reply->user->avatar))

                                      @else

                                      @endif
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="media-heading">Posted by {{ $reply->user->name }}   <small>posted on: {{ $reply->created_at }}</small> </h4>
                                    {{ $reply->reply}}
                                  </div>
                                </div>
                          @endif
                          @empty

                          @endforelse
                        </div>
                        <div class="cmForm">
                          <form class="form-horizontal commentForm" role="form" method="POST" action="/talents/comments" enctype="multipart/form-data">
                            <div class="cmerrors">

                            </div>
                            @if(Session::has('message'))
                              {!! Session::get('message') !!}
                            @endif
                              {{ csrf_field() }}
                              <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                                  <div class="col-md-11">
                                      <textarea id="comment" type="text" rows="2" class="form-control" name="comment" required>{{ old('comment') }}</textarea>
                                      @if ($errors->has('comment'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('comment') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                              <input type="hidden" name="post_id" class="postId" value="{{ $post->id }}">
                              <button type="submit" class="btn btn-primary submit commentBtn">
                                 <i class="fa fa-comment" aria-hidden="true"></i> Comment
                              </button>
                          </form>
                        </div>
                        <script type="text/javascript">
                          //Setting token
                          var token = '{{ Session::token() }}';
                          var url = '{{ route('comment') }}';
                          var user = '{{ Auth::User()->name }}';
                        </script>
                      </div>
                    </div>
                  @empty

                  @endforelse

                  {{ $talents->links() }}
                </div>
        </div>
        </div>
      </div>

    </div>

  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
