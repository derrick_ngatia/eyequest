@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong> YOUR TRANSACTIONS </strong> </h5>
      <div class="row">
        @forelse($payments as $payment)
        <div class="panel panel-default">
          <div class="panel-heading">
              <strong>Post Name: </strong> {{ $payment->post->product_name }}
          </div>
          <div class="panel-body">
            <div class="col-md-6">
              @if(isset($payment->post->image))
                <img src="{{ url('/Eyequest/public/uploads/posts/'. $payment->post->image )  }}" alt="{{ $payment->post->description }}" class="post-img thumbnail" />
              @else
                <img src="{{ url('images/logo/eyquest.jpg')  }}" alt="{{ $payment->post->description }}" class="post-img thumbnail" />
              @endif
            </div>
            <div class="col-md-6">
              <h5 class="head"><strong>Transaction Details</strong></h5>
              <strong>Mode of Payment: </strong> Mpesa <br>
              <strong>Phone Number: </strong> {{ $payment->phone }} <br>
              <strong>Amount: </strong> {{ $payment->amount }} <br>
              <strong>Payment Date: </strong> {{ $payment->created_at }} <br>
              <strong>Status: </strong> @if($payment->confimed == 1)
                                            <span class="confirmed"> Payment confirmed</span>
                                        @elseif($payment->failed == 1)
                                            <span class="failed">Invalid Payment</span>
                                        @else
                                            <span class="waiting">Payment waiting confirmation</span>
                                        @endif
                                        <br>

            </div>
          </div>

        </div>

        @empty
          <div class="col-md-12">
            <p>You have not made any transactions yet</p>
          </div>
        @endforelse
        <div class="pagination">
          {{ $payments->links() }}
        </div>
      </div>

    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
