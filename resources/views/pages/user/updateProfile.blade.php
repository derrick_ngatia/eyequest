@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update Your profile
                    <div class="pull-right">

                    </div>
                </div>
                <div class="panel-body">

                  <form class="form-horizontal" role="form" method="POST" action="/updatingprofile" enctype="multipart/form-data">

                      @if(Session::has('message'))
                        {!! Session::get('message') !!}
                      @endif
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Your Name</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                      <div class="form-group{{ $errors->has('talent') ? ' has-error' : '' }}">
                          <label for="talent" class="col-md-4 control-label"> Skill/Occupation &#38; Talent </label>
                          <div class="col-md-6">
                              <input id="talent" type="text" class="form-control" name="talent" value="{{ $user->talent }}" required>
                              @if ($errors->has('talent'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('talent') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                      <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                          <label for="description" class="col-md-4 control-label">Description Yourself</label>
                          <div class="col-md-6">
                              <textarea id="description" type="text" rows="4" class="form-control" name="description" value="" required>{{ $user->cv }}</textarea>
                              @if ($errors->has('description'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('description') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                      <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                          <label for="category" class="col-md-4 control-label">Employment Status</label>
                          <div class="col-md-6">
                            <select class="form-control" name="employed" value="{{ old('employed') }}" required >
                              <option value="2">SELF-EMPLOYED</option>
                              <option value="1">EMPLOYED</option>
                              <option value="0">UNEMPLOYED</option>
                          </select>
                              @if ($errors->has('employed'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('employed') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <label for="name" class="col-md-4 control-label">Profile Image</label>
                          <div class="col-md-6">
                            <span class="btn btn-default btn-file">
                              Profile Image <input type="file" name="image" class="" required="true">
                            </span>
                              @if ($errors->has('image'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('image') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary submit">Save changes</button>
                                <button type="reset" class="btn btn-default" >Reset</button>
                            </div>
                        </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
