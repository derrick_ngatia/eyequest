@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong>YOUR POSTS</strong> </h5>
      <div class="row">
        @forelse ($bids as $bid)

        <div class="col-md-4">
          <div class="post-wrapper">
            <div class="image-description-wrapper">
              <div class="post-image-wrapper">
                @if(isset($bid->post->image))
                  <img src="{{ url('/Eyequest/public/uploads/posts/'. $post->image )  }}" alt="{{ $bid->post->description }}" class="post-img" />
                @else
                  <img src="{{ url('images/logo/eyquest.jpg')  }}" alt="{{ $bid->post->description }}" class="post-img" />
                @endif
              </div>
              <div class="post-description">
                <h2 class="heading"><strong> {{ $bid->post->product_name }}</strong></h2>
                <p>{{ $bid->post->description }}</p>
              <div class="shopping-cart">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i> ({{ $bid->post->bidders }})Bid
              </div>
              </div>

            </div>
              <div class="links-wrapper">
                <a href="/Suggestions/{{ $bid->post->id}}"><i class="fa fa-plus-square" aria-hidden="true"></i> suggections({{ $bid->post->suggestions }}) </a>  &nbsp;
                <a href="/Bidders/{{ $bid->post->id}}"><i class="fa fa-plus-square" aria-hidden="true"></i> Bidders({{ $bid->post->bidders }})</a>
              </div>


          </div> <!-- post-wrapper -->
        </div> <!-- col-md-4 -->
            @empty
                <p>No posts yet</p>
            @endforelse

            <div class="pagination">
              {{ $bids->links() }}
            </div>
        </div>
    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
