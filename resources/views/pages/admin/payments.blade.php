@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong> PAYMENTS </strong> </h5>
      <div class="row">
        <div class="col-md-11">
          <h6> <strong>MPESA PAYMENTS</strong></h6>
          <table class="table">
            <thead>
              <tr>
                <th>Transaction Code</th>
                <th>Amount</th>
                <th>Phone</th>
                <th>Confirmation </th>
              </tr>
            </thead>
            <tbody>
              @forelse($payments as $payment)
              <tr>
                <td>{{ $payment->transactionCode }}</td>
                <td>{{ $payment->amount }}</td>
                <td>{{ $payment->phone }}</td>
                <td>
                    @if($payment->confirmed == 1)
                      <button type="button" name="button" class="btn btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>APPROVED</button>
                    @elseif($payment->failed == 1)
                      <button type="button" name="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>CANCELLED</button>
                    @else
                      <div class="buttons">
                        <a href="/approve/{{ $payment->id}}" class="approve"><button class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>APPROVE</button></a>
                        <a href="/cancel/{{ $payment->id}}" class="cancel"><button class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>CANCEL</button></a>
                      </div>
                    @endif
                </td>
              </tr>
              @empty

              @endforelse
            </tbody>
          </table>
            {{ $payments->links() }}
        </div>
      </div>
      <div class="row">
        <div class="col-md-11">
          <h6><strong>Other forms of payments</strong></h6>

        </div>
      </div>

    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
