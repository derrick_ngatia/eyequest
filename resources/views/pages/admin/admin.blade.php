@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong> DASHBOARD </strong> </h5>
      <div class="row">
        <div class="col-md-11">
          <h6 class="heading"> <strong>ACCOUNT HOLDERS</strong></h6>
          <table class="table">
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Location</th>
            </tr>
            @forelse($users as $user)
              <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone }}</td>
                <td>{{ $user->location }}</td>
              </tr>
            @empty
              <tr>
                No users yet
              </tr>
            @endforelse
          </table>

          {{ $users->links() }}

        </div>
      </div>
      <div class="row">
        <div class="col-md-11">
          <h6 class="heading"><strong>Messages From Users</strong></h6>

              @forelse($messages as $message)
              <div class="row messages">
                <div class="col-md-6">
                  <strong>User Details</strong><br>
                <strong>Name: </strong>  {{ $message->name}}<br>
                <strong>Email: </strong>  {{ $message->email }}
                </div>

                <div class="col-md-6">
                  <strong>Message Details</strong><br>
                  <strong>Subject: &nbsp;</strong>{{ $message->subject }} <br>
                  <strong>Message: &nbsp;</strong>{{ $message->messages }} <br>
                </div>
                <br>
                </div>
              @empty
               <p>No messages yet</p>
              @endforelse
              {{ $messages->links() }}

        </div>
      </div>
      <div class="row">
        <div class="col-md-11">
          <h6 class="heading"><strong> Statistics </strong></h6>
        </div>
      </div>
      <div class="col-md-11">
        <form class="form-horizontal" role="form" method="POST" action="/admin/advert" enctype="multipart/form-data">
          @if(Session::has('message'))
            {!! Session::get('message') !!}
          @endif
            {{ csrf_field() }}

              <span class="btn btn-default btn-file">
                Change Advert Picture <input type="file" name="image" class="" required="true">
              </span>

              <button type="submit" class="btn btn-primary submit uploadProf">
                  Change Profile
              </button>
        </form>
      </div>
    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
