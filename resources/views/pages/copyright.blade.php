@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      @include('pages.category')
    </div>
    <div class="col-md-9">
      <h5 class="heading"> <strong> COPYRIGHT </strong> </h5>
      <div class="row">
        <div class="col-md-11">

          <p> EyeQuest is registered as EYEQUEST TECHNOLOGIES under the registration of Business Names Act.
             As stipulated under the company policy, names, content, images and logos identifying eyequest
             applications and associated companies shall not be  duplicated or  implemented or otherwise any
             license or right under any trademarks or patent of eyequest, its associated companies or any other party.
              All copyright, trademarks and other intellectual property rights in this application( including the
               design, arrangement , look and feel) and all material or content supplied as part of our applications and
               services shall remain at all times the property eyequest or eyequest’s licensors. In accessing eyequest application,
                you may not agree to, permit or assist in any way any third party copy, reproduce, download, post, store, distribute,
                transmit, broadcast, commercially exploit or modify on any way the material or content without account holder written
                permission. </p>


        </div>
      </div>


    </div>
  </div>

  <div class="row">
    @include('layouts.footer')
  </div>

</div>


@endsection
