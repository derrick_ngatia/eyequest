@component('mail::message')
# Welcome {{ Auth::User()->name }} to EyeQuest

We sent you this email because an account was create with this email address <br>

<?php
      $id = Auth::User()->id;
      $token = Auth::User()->token;
      $url = "https://eyequest.co.ke/confirmEmail/". $id . "/". $token ;
?>

@component('mail::button', ['url' => $url, 'color' => 'red'])
Confirm Your Email Here
@endcomponent

If the button does not work copy the link below to your browser <br>
	{{ $url }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
