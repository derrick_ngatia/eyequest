@component('mail::message')
# Hello {{ $recommendation->post->user->name }}, <br>

#### {{ $recommendation->user->name }} Recommended you on your post <br>

<?php
  $pathToFile = public_path('\uploads\cvposts\\'. $recommendation->post->image)

?>

@if(isset($reply->post->image))
  @component('mail::panel')
    <img src="data:image/png;base64,{{base64_encode(file_get_contents(public_path('./uploads/cvposts/'. $recommendation->post->image)))}}" alt="">
    {{ $recommendation->post->description }}
  @endcomponent
@else
  @component('mail::panel')
    {{ $recommendation->post->description }}
  @endcomponent
@endif

Thanks,<br>
{{ config('app.name') }}
@endcomponent
