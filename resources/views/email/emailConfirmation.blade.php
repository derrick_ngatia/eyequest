@component('mail::message')
# Welcome {{ $user->name }} to EyeQuest

You recieved this email because an account was create with this email address <br>
<?php
      $id = $user->id;
      $token = $user->token;
      $url = "https://eyequest.co.ke/confirmEmail/". $id ."/". $token;
?>

@component('mail::button', ['url' => $url, 'color' => 'red'])
Confirm Your Email Here
@endcomponent

If the button does not work copy the link below to your browser <br>
	{{ $url }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
