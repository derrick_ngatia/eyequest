@component('mail::message')
# Hello {{ $reply->post->user->name }}, <br>

#### {{ $reply->user->name }} commented on your post <br>
{{ $reply->reply }}

<?php
  $pathToFile = public_path('\uploads\cvposts\\'. $reply->post->image)

?>

@if(isset($reply->post->image))
  @component('mail::panel')
    <img src="data:image/png;base64,{{base64_encode(file_get_contents(public_path('./uploads/cvposts/'. $reply->post->image)))}}" alt="">
    {{ $reply->post->description }}
  @endcomponent
@else
  @component('mail::panel')
    {{ $reply->post->description }}
  @endcomponent
@endif



Thanks,<br>
{{ config('app.name') }}
@endcomponent
