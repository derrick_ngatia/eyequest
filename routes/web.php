<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailConfirmation;
use App\Mail\ResendConfirmation;
use App\User;


Route::get('/sendMails', function(){
  $users = User::all();

  foreach ($users as $user) {
      Mail::to($user->email)->send(new EmailConfirmation($user));
  }

  echo "Done emailing";
});

Route::get('/tokens', function() {
  $user = User::all();

  foreach ($user as $user) {
    $token = str_random(40);
    $user->token = $token;
    $user->save();
  }
});
Route::get('/confirmEmail/{id}/{token}', function ($id, $token){
  echo $id . " ". $token;
  $user = User::where('id', $id)
                ->where('token', $token)
                ->first();
    if($user){
      $user->activated = 1;

      if($user->save()){
        return redirect('/');
      }else{
        $sucess = '<div class="alert alert-danger alert-dismissable"> Failed to confirm the email at the moment, Please try again </div>';
        Session::flash('sent', $sucess);
        return redirect('/confirm');
      }
    }else {
      $sucess = '<div class="alert alert-danger alert-dismissable"> Invalid url, If the problem persists, resend the email </div>';
      Session::flash('sent', $sucess);
      return redirect('/confirm');
    }

});
Route::get('/categories', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/home/{id}', 'HomeController@categories');
Route::post('/search', 'HomeController@search');

Route::post('/updatingprofile', 'UpdateProfileController@updateProfile');
Route::get('/updateprofile', function() {
  $user = Auth::User();
  return view('pages.user.updateProfile', compact('user'));
});

Route::get('/confirm', function (){
	if (Auth::User()->activated == 1) {
      	   return redirect('/');
    	}
    return view('pages.confirm');
});
Route::get("/resendConfirmation", function () {
  Mail::to(Auth::User()->email)->send(new ResendConfirmation(Auth::User()));
  $sucess = '<div class="alert alert-success alert-dismissable"> Email has been sent, check your email </div>';
  Session::flash('sent', $sucess);
  return redirect('/confirm');
});

/*Route::get('/info', function (){
	echo phpinfo();
}); */
/*Route::get('/safe', function (){
if( ini_get('safe_mode') )
{
  echo "Safe mode is enabled";
}else
{
  echo "Safe mode is disabled";
}

});*/

Route::group(['middleware' => ['auth', 'admin', 'confirmation']], function(){
  Route::get('/admin', 'admin\\AdminController@index');
  Route::get('/payments', 'admin\\AdminController@payments');
  Route::get('/admin/manage', 'admin\\AdminController@index');
  Route::get('/approve/{id}', 'admin\\AdminController@approve');
  Route::get('/cancel/{id}', 'admin\\AdminController@cancel');
  Route::post('/admin/advert', 'admin\\AdminController@adertPan');
});



Route::get('/profession', 'TalentsController@index');
Route::get('/', 'TalentsController@index');
Route::post('/talents', 'TalentsController@create')->name('post');
Route::post('/talents/comments', 'TalentsController@create_comments')->name('comment');
Route::post('/talents/question', 'TalentsController@post_Question')->name('question');
Route::get('/talents/recommend/{id}', 'TalentsController@post_recommend')->name('recommend');
Route::get('/talents/approve/{reply_id}/{user_id}', 'TalentsController@reply_approve')->name('approve');
Route::get('/follow/{id}', 'TalentsController@follow')->name('follow');


Route::get('/contact', function (){
  return view('pages.contact');
});

Route::get("/copyright", function() {
	return view('pages.copyright');
});

Route::post('/contact', 'PostsController@message');

Route::get('/about-us', function (){
  return view('pages.about-us');
});

Auth::routes();

Route::get('/email', function(){
    Mail::send('email.test', ['name' => 'jqhn'], function ($message){
      $message->to('johnmaina109@gmail.com', 'some Guy')->subject('welcome');
      echo "email sent";
    });
});
Route::get('/lost-and-found', 'LostAndFoundController@show');
Route::get('/lost-and-found/add', 'LostAndFoundController@create')->middleware('auth');
Route::post('/lost-and-found/add', 'LostAndFoundController@add')->middleware('auth');
Route::get('/lost/{id}', 'LostAndFoundController@get_single');
/*Route::get('/confirm-lost/{id}', 'LostAndFoundController@confirmPayment');
Route::get('/payClaim/{id}', 'LostAndFoundController@confirmPayment'); */
Route::get('/lost-and-found/pay/{id}', 'LostAndFoundController@confirmPayment');
Route::post('/save_lost', 'LostAndFoundController@savePayment');




Route::get('/post/add', 'PostsController@add');
Route::post('/create/add', 'PostsController@create');
Route::post('/save_bid', 'PostsController@add_bid');
Route::get('/Bidders/{id} ', 'PostsController@Bidders');
Route::get('/Suggestions/{id} ', 'PostsController@Suggestions');
Route::post('/save_suggestion', 'PostsController@add_suggestion');
Route::get('/Bid-item/{id}', 'PostsController@bidItem');
Route::get('/suggest-item/{id}', 'PostsController@suggestItem');


Route::get('/user', 'UsersController@getUser');
Route::get('/user/bids', 'UsersController@get_bids');
Route::get('/user/transactions', 'UsersController@get_transactions');
Route::get('/profile', 'UsersController@profile');
Route::post('/profile', 'UsersController@create_prof');
Route::post('/profile/edit', "UsersController@edit_profile");
Route::get('/UserProfile/{name}/{id}', 'UsersController@get_user');

//Routes for mail quick preview
Route::get('/mailable', function () {
    $recommendation = App\Recommend::find(11);

    return new App\Mail\RecommendedEmail($recommendation);
});

Route::get('/mailUser', function () {
    $user = App\User::find(1);

    return new App\Mail\ConfirmationEmail($user);
});

Route::get('/mailReply', function () {
    $reply = App\Replies::find(1);

    return new App\Mail\CommentEmail($reply);
});
